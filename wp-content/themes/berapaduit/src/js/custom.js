var $ = jQuery;
jQuery.fn.hasAttr = function(name) {
    return this.attr(name) !== undefined;
};

// jQuery toggle text
jQuery.fn.extend({
    toggleText: function (a, b){
        var that = this;
            if (that.text() != a && that.text() != b){
                that.text(a);
            }
            else
            if (that.text() == a){
                that.text(b);
            }
            else
            if (that.text() == b){
                that.text(a);
            }
        return this;
    }
});

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

$(document).ready(function($){

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	LIVE SEARCH
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('.bd-search').each(function(){
		var thefield = $(this);
		var resultwrapper = $(this).find('.bd-live-search');
		var dismiss = $(this).find('.live-search-dismiss');

		thefield.find('input').on('keyup', function(e){
			var thisval = $(this).val();
			if( thisval && e.which <= 90 && e.which >= 48 ) {
				//console.log(thisval);
				delay(function(){
				    jQuery.ajax({
						type: "post",
						url: berapaduit.ajaxurl,
						dataType: 'json',
						data: {
				            'action': 'bd-live-search',
				            'nonce': berapaduit.nonce,
				            'search': thisval
				        },
				        beforeSend: function(data) {
				        	$(resultwrapper).show(0, function(){
				        		$(this).html('<li class="p-3 text-center"><a href="#"><i class="fa fa-spinner fa-spin mr-0"></i> Loading</a></li>');
				        	});
				        	$(dismiss).show();
				        },
						success: function (data) {
							//console.log(data);
							$(resultwrapper).html($(data['html']));

							$(dismiss).on('click', function(){
								$(this).hide();
								$(resultwrapper).html('').hide();
								$(thefield).find('input').val('');
							});
							currentterm = thisval;
						},
						error: function(errorThrown){
							alert(errorThrown);
						} 
					});
				}, 1000 );
			}		

			return false;
		});
	});

	$('.bd-m-search-form').prependTo('#navbarNavDropdown');

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	Footer toggle
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('.foogle').click(function(e){
		e.preventDefault();
		$('.footer-main').slideToggle(500, function(){
			//$("html, body").animate({ scrollTop: $(document).height() }, 500);
		});
		$(this).find('.netralclr').text(function(i, text){
        	return text === "Show Footer" ? "Hide Footer" : "Show Footer";
      	});
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	BALLOON
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('[data-toggle="popover"]').popover({
		html: true
	});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});

	var lastScrollTop = 0;
	$(window).scroll(function(event){
	   var st = $(this).scrollTop();
	   if (st > lastScrollTop){
	       	$('.footer-nav').show();
	   } else {
	      	$('.footer-nav').hide();
	   }
	   lastScrollTop = st;
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	Scroll to top
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('.smoothtop').on('click', function(e) {
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "fast");
		return false;
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	LOGIN TAB
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('#show_login').on('click', function(e){
		e.preventDefault();
		$('.signup').toggle();
		$('.signin').toggle();
		$(this).toggleText('Already have an Account?', 'Register Here');
	});

	/*$('.bd-login-modal').on('show.bs.modal', function (event) {
		var modal = $(this);
		modal.find('.signup').find('input').not('input[type=button]').val('');
		$('.rm-form-field-invalid-msg').hide(0, function(){
			$(this).remove();
		});
	});*/

	if( $('.zl_links.loggedout').length ) {
		var status = berapaduit.userstatus;
		$('.zl_links.loggedout').find('a.comment-reply-login').on('click', function(e){
			e.preventDefault();
			$(this).attr('href', '#');
			$('.bd-login-modal').modal('show');
		});

		if( 'loggedout' == status ) {
			$(this).attr('href', '#');
			$('.must-log-in').find('a').on('click', function(e){
				e.preventDefault();
				$(this).attr('href', '#');
				$('.bd-login-modal').modal('show');
			});			
		}
	}

	var status = berapaduit.userstatus;
	if( 'loggedout' == status ) {
		$(this).attr('href', '#');
		$('.must-log-in').find('a').on('click', function(e){
			e.preventDefault();
			$(this).attr('href', '#');
			$('.bd-login-modal').modal('show');
		});			
	}

	//$('.rminput input').addClass('form-control').attr('style', '');

	$('.rminput').each(function(){
		$(this).find('input').addClass('form-control').attr('style', '');
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	TINYMCE LIVE PREVIEW
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if( $('.wpuf-form-add').length ) {
		$('#datepicker_59').datepicker({
			dateFormat: 'yy-m-d',
			onSelect: function(date) {
		        $('input[name="date_expires"]').val(date);
		    }
		});

		$('#post_title_59').on('keyup', function(){
			var thisval = $(this).val();
			$('.pre-title').text(thisval);
		});

		window.onload = function () {
	        tinymce.get('post_content_59').on('keyup',function(e){
	        	//console.log(this.getContent());
	        	$('.entry-content').html(this.getContent());
	        });
    	}
	}

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	LINK PREVIEW
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	var apiKey = '5a5d8ae2ae29eae465204598'; // <-- Replace with your AppId	

	$('input[name=external_link]').next('.wpuf-wordlimit-message').before('<div class="img-preview deallink-preview"></div>');

	var url_1 = $('input[name=external_link]').val();

	if( url_1 ) {
		var urlEncoded_1 = encodeURIComponent(url_1);
		// The entire request is just a simple get request with optional query parameters
		var requestUrl_1 = 'https://opengraph.io/api/1.1/site/' + urlEncoded_1 + '?app_id=' + apiKey;
		$.getJSON(requestUrl_1, function( json ) {
		    $('.img-preview').html('<img src="'+json.hybridGraph.image+'" alt=""/>');

		    $('input[name=external_image]').val(json.hybridGraph.image);
	  	});
	}
	
	$('input[name=external_link]').on('keyup', function(){
		var url = $(this).val();
		var urlEncoded = encodeURIComponent(url);
		// The entire request is just a simple get request with optional query parameters
		var requestUrl = 'https://opengraph.io/api/1.1/site/' + urlEncoded + '?app_id=' + apiKey;
		$.getJSON(requestUrl, function( json ) {
			//console.log(JSON.stringify(json));
		    $('.img-preview').html('<img src="'+json.hybridGraph.image+'" alt=""/>');

		    $('input[name=external_image]').val(json.hybridGraph.image);
	  	});
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	ADD CUSTOM TERM: GROUP ITEM
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if( $('.group_item_hack').length ) {
		$('.group_item_hack').find('.wpuf-fields').append('<input class="add-group-item-input form-control" type="text" name="group_item"/><a href="" class="btn btn-primary add-group-item"><i class="fa fa-plus mr-0"></i></a>');
	}

	function checkuncheck(){
		$(".group_item_hack input[type='checkbox']").change(function() {
			var row = $(this).closest('li');
		    if(this.checked) {
		    	row.addClass('is-checked');
		    	row.insertBefore( row.closest('.wpuf-category-checklist').find('li:first-of-type') );
		    } else {
		    	row.removeClass('is-checked');
		    	row.insertAfter( row.closest('.wpuf-category-checklist').find('li:last-of-type') );
		    }
		});
	}

	checkuncheck();

	$(".add-group-item").click(function (e) {
		e.preventDefault();
		button = $(this);
		var tempat = $('.group_item_hack').find('.wpuf-category-checklist');
		var groupitem = $('.add-group-item-input').val();
		button.html('<i class="fa fa-spinner fa-spin mr-0"></i>');

		jQuery.ajax({
			type: "post",
			url: berapaduit.ajaxurl,
			dataType: 'html',
			data: {
	            'action': 'add-term',
	            'nonce': berapaduit.nonce,
	            'group_item': groupitem
	        },
			success: function (data) {
				//console.log(data);
				var id = data;
				tempat.append('<li id="group-'+id+'"><label class="selectit"><input class="" value="'+id+'" type="checkbox" name="group[]" id="in-group-'+id+'"> '+groupitem+'</label></li>');
				button.html('<i class="fa fa-plus mr-0"></i>');
				checkuncheck();
			},
			error: function(errorThrown){
				alert(errorThrown);
			} 
		});

		return false;
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	Sticky Bar preview Deals
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if( $('#stickysidebar') && $(window).width() >= 860 ) {
		var footerheight = $('.footer-wrapper').outerHeight();
		var footerheight = footerheight + 15;
		$("#stickysidebar").sticky({
			topSpacing:100,
			bottomSpacing: footerheight
		});
	}

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	COVER USER
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if( $('.user-cover').length ) {
		var imgurl = $('.user-cover').find('.up-ava').find('img').attr('src');
		//console.log(imgurl);
		$('.user-cover-placeholder').css('background-image', 'url('+ imgurl +')');
	}

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	LAYOUT SWITCHER
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if( $('.layoutfilter').length ) {
		$('.layoutfilter a').each(function(){
			$(this).on('click', function(){
				var btn = $(this),
					lain = $(this).siblings('a').attr('data-layout'),
					layout = btn.attr('data-layout');
				/*if( $('.site-main').hasClass('grid') ) {
					$('.site-main').toggleClass('grid default');
				} else {
					$('.site-main').addClass(layout);
				}*/
				btn.addClass('active').siblings('a').removeClass('active');
				$('.site-main').addClass(layout).removeClass(lain);

				jQuery.cookie('loopview', layout, {
					path: '/',
					expires: 999
				});
			});
		});

		if( $.cookie('loopview') == 'grid' ) {
			$('.layoutfilter a[data-layout="grid"]').addClass('active').siblings('a').removeClass('active');
		}
	}

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	POST VOTE
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	function post_voting() {
		$(".post-like a").click(function () {
			heart = $(this);
			post_id = heart.data("post_id");

			jQuery.ajax({
				type: "post",
				url: berapaduit.ajaxurl,
				data: "action=post-like&nonce=" + berapaduit.nonce + "&post_like=&post_id=" + post_id,
				success: function (count) {
					//console.log(count);
					if (count.indexOf("already") !== -1) {
						var lecount = count.replace("already", "");
						if (lecount == 0) {
							var lecount = "0";
						}
						heart.children(".like").removeClass("pastliked").addClass("disliked");
						heart.children(".count").removeClass("liked").addClass("disliked").text(lecount);
					} else {
						heart.children(".like").addClass("pastliked").removeClass("disliked");
						heart.children(".unliker").html("<i class='fa fa-no-alt'></i>");
						heart.children(".count").addClass("liked").removeClass("disliked").text(count);
					}
				}
			});

			return false;
		});

		$(".post-down a").click(function () {
			heart = $(this);
			post_id = heart.data("post_id");

			jQuery.ajax({
				type: "post",
				url: berapaduit.ajaxurl,
				data: "action=post-down&nonce=" + berapaduit.nonce + "&post_down=&post_id=" + post_id,
				success: function (count) {
					//console.log(count);
					if (count.indexOf("downed") !== -1) {
						var lecount = count.replace("downed", "");
						if (lecount == 0) {
							var lecount = "0";
						}
						//heart.children(".unliker").text("");
						heart.parent('.post-down').prev('.post-like').find(".count").removeClass("liked").addClass("disliked").text(lecount);
					} else {
						heart.parent('.post-down').prev('.post-like').find(".count").addClass("liked").removeClass("disliked").text(count);
					}
				}
			});

			return false;
		});
	}

	// CALL the post voting
	post_voting();

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	POST EXPIRED
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$(".post-expires a").click(function (e) {
		e.preventDefault();
		heart = $(this);
		post_id = heart.data("post_id");

		jQuery.ajax({
			type: "post",
			url: berapaduit.ajaxurl,
			data: "action=post-expires&nonce=" + berapaduit.nonce + "&post_expires=&post_id=" + post_id,
			success: function (count) {
				//console.log(count);
				if (count.indexOf("already") !== -1) {
					var lecount = count.replace("already", "");
					if (lecount == 0) {
						var lecount = "0";
					}
					
				} else {
					$('.expired-notif').addClass('muncul');
					setTimeout(function() {
						$('.expired-notif').removeClass('muncul');
					}, 2000);
					heart.addClass('text-primary').html('<i class="fa fa-clock-o"></i> Expired</span>');
				}
			}
		});

		return false;
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	NOTIFICATIONS
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	var homeurl = berapaduit.siteurl,
		notifnumb = '_notifnumb',
		cookey = homeurl.concat(notifnumb),
		notif = $('.notifwrapper .dropdown-menu'),
		notifitems = notif.find('.notif-item').length;

	if( $.cookie(cookey) < notifitems ) {
		// console.log($.cookie(cookey));
		var newnotif = parseInt( notifitems - $.cookie(cookey) );
		$('.notif-item').slice(0,newnotif).addClass('unread');
		$('.notifwrapper .btn.text-white').append('<span class="badge badge-pill badge-danger notifications-peak">'+newnotif+'</span>');
	}

	if( !$.cookie(cookey) ) {
		$('.notifwrapper .btn.text-white').append('<span class="badge badge-pill badge-danger notifications-peak">'+notifitems+'</span>');
	}

	$('.notif-only').on('shown.bs.dropdown', function () {
	  	$('.notifications-peak').hide(0, function(){
	  		jQuery.cookie(cookey, notifitems, {
	  			path: '/',
	  			expires: 365
	  		});
	  	});
	});

	$('.notif-only').on('hidden.bs.dropdown', function () {
	  	$('.notif-item.unread').removeClass('unread');
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HOT POSTS SLIDESHOW
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('.hotpost.is_today').slick({
		rows: 4,
		arrows: false,
		dots: true,
		adaptiveHeight: true
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HOT POST TOGGLE
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('#hotpostselect').on('change', function(){
		var status = this.value;
		if( status === 'week' ) {
			$('.is_week').show().siblings('.hotpost').slick('unslick').hide(0, function(){
				setTimeout(function() {
					$('.is_week').slick({
							rows: 4,
							arrows: false,
							dots: true,
							adaptiveHeight: true
						});
				}, 100);
			});
				
		} else if( status === 'today' ) {
			$('.is_today').toggle().siblings('.hotpost').slick('unslick').toggle(0, function(){
				setTimeout(function() {
					$('.is_today').slick({
						rows: 4,
						arrows: false,
						dots: true,
						adaptiveHeight: true
					});
				}, 100);
			});
		}
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	AJAX LOAD MORE
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */	
	function detectpaging() {
		// Assuming that all images have set the 'data-src' attribute instead of the 'src'attribute
		$(".paging-detect").each(function(e) {
			var ini = $(this);

			ini.on('inview', function(event, isInView){
				if (isInView) {
					var page = parseInt( ini.attr('data-paged') );
					setTimeout(function() {
						$('.pg-nm').text(page);
					}, 200);
					//console.log('test: '+ page);
				} 
			});
		});
	}

	$('.wishbone_loadmorebutton').click(function(e){
		e.preventDefault();
		var button = $(this),
			looptype = button.attr('data-looptype'),
			perpage = button.attr('data-perpage'),
			targetoffset = button.attr('data-targetoffset'),
			currentpaged = button.attr('data-currentpaged'),
			year = button.attr('data-year'),
			month = button.attr('data-month'),
			day = button.attr('data-day'),
			author = button.attr('data-author'),
			search = button.attr('data-search'),
			tag_id = button.attr('data-tag'),
			orderby = button.attr('data-orderby'),
			daysago = button.attr('data-daysago'),
			discussed = button.attr('data-discussed'),
			maxpages = button.attr('data-maxpages');

		if(button.hasAttr('data-cat')) {
	        var cat = button.attr('data-cat');
	    }

		// Prevent operation in the last page
		if( maxpages == currentpaged ){
			return false;
		}

		jQuery.ajax({
			type: 'POST',
			url: berapaduit.ajaxurl,
			data: {
				"action": "wishbone_loadmore",
				"looptype": looptype,
				"perpage": perpage,
				"currentpaged": currentpaged,
				"cat": cat,
				"targetoffset": targetoffset,
				"loadmore": berapaduit.loadmore,
				"loading": berapaduit.loading,
				"caughtup": berapaduit.caughtup,
				"year": year,
				"month": month,
				"day": day,
				"search": search,
				"tag_id": tag_id,
				"orderby": orderby,
				"daysago": daysago,
				"discussed": discussed,
				"author": author,
			},
			dataType: 'json',
			beforeSend: function(data) {
				button.text('Loading...');
				$('.site-main').find('article:not(.alreadyloaded)').addClass('alreadyloaded');
			},
			success: function(data) {

				$(data['html']).insertBefore(button);

				// Update the button
				var newcurrentpaged = parseInt(currentpaged) + 1;
				button.attr('data-currentpaged', newcurrentpaged);
				$('.pg-nm').text(newcurrentpaged);

				// In the last page, make the button unclickable
				if( maxpages == newcurrentpaged ){
					// button.text(berapaduit.caughtup).addClass('isdead');
					/*button.hide(0, function(){
						button.remove();
					});*/
					button.text('No more content');
				} else {
					button.text(berapaduit.loadmore);
				}

				post_voting();
				detectpaging();
			},
			error: function(data){
				button.text('ERROR!');
			}
		});
	});

	$('.wishbone_loadmorebutton').on('inview', function(event, isInView) {
		if (isInView) {
			$('.wishbone_loadmorebutton').trigger('click');
		} 
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	COMMENT PERMALINK COPIER
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('#commentPermalink').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var recipient = button.data('permalink');
		var modal = $(this);
		modal.find('.modal-body input').val(recipient);
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
	Copier
	oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	$('#copier').on('click', function(e){
		e.preventDefault();
		var input1 = document.getElementById('permalinkinput');
		input1.select();
		document.execCommand('copy');
	});

	/*$('.comment-reply-link').on('click', function(){
		var btn  = $(this);
		var content = btn.parent('li.comment').find('.zl_comment_content');

		btn.parent('li.comment').find('textarea').text('<blockquote></blockquote>');
	});*/
});