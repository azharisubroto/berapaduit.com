<article class="card bd-card mb-3">	
	<div class="card-body">
		<div class="row">
			<div class="col-md-12">
				<h2 class="entry-title">There's no such post</h2>
				<div class="entry-content">
					<p>The post that you're looking for is not found, try someting else please :)</p>
				</div>
			</div>
		</div>
	</div>
</article>