<?php
/**
 * Loop template
 *
 * @package understrap
 */

if( ! is_user_logged_in() ) {
	$logbutton = '<a href="" class="btn-ghost" data-toggle="modal" data-target=".bd-login-modal"></a>';
} else {
	$logbutton = '';
}

if( is_user_logged_in() ) {
	global $current_user;
	wp_get_current_user();
	$user = get_current_user_id();
}
bd_set_image( get_the_ID() );
?>
<article <?php post_class('card bd-card mb-3'); ?> id="post-<?php the_ID(); ?>">	
	<div class="card-body">
		<div class="row">
			<div class="col-md-3">
				<div class="item-thumb">
					<?php 
						if( has_post_thumbnail() ) {
							the_post_thumbnail('medium');
						} else {
							$image = get_post_meta( get_the_ID(), 'external_image', true );
							if( $image ) {
								echo '<img src="'.$image.'" alt="'.get_the_title().'" />';
							} else {
								echo '<img src="'.get_template_directory_uri().'/img/no-image.jpg" alt="'.get_the_title().'" />';
							}							
						}
					 ?>
				</div>
			</div>
			<div class="col-md-9 mt-3 mt-3 mt-sm-0">
				<div class="row mb-2">
					<div class="col-md-6 col-6">
						<div class="deal-vote">
							<?php echo getPostLikeLink( get_the_ID() ); echo $logbutton; ?>
						</div>
					</div>
					<div class="col-md-6 col-6 text-right fadetext small">
						<i class="fa fa-clock-o"></i> <?php printf( _x( '%s ago', '%s = human-readable time difference', 'your-text-domain' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
					</div>
				</div>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="clearfix entry-meta">
					<?php 
						if( get_field('price') ):
							echo '<strong class="text-green">Rp '; 
							echo number_format( get_field('price') );
							echo '</strong> | ';
						endif; 
					?>
					<?php the_terms( get_the_ID(), 'deal', '', ', ', '' ); ?> Deal
				</div>
				<div class="entry-content">
					<p><?php echo bd_excerpt( 23 ); ?></p>
				</div>
				<div class="entry-footer">
					<div class="inner">
						<div class="row">
							<div class="col-md-4 col-12 text-medium">
								<div class="ava inline mr-1">
									<?php echo get_avatar( get_the_author_meta('ID') , 25 ); ?>
								</div>
								<?php the_author_posts_link(); ?>
								<?php 
									if( is_user_logged_in() && get_the_author_meta('ID') === $user ) {
										$nonce = wp_create_nonce( 'wpuf_edit' );
										$pid = get_the_ID();
										echo '&nbsp;  <a href="'.home_url().'/submit-form/?pid='.$pid.'&_wpnonce='.$nonce.'"><i class="fa fa-pencil"></i> edit</a>';
									}
								 ?>
							</div>
							<div class="col-md-8 col-12 text-right entry-footer-buttons">
								<a href="<?php the_permalink(); ?>#comments" class="btn btn-sm btn-outline-dark bd-btn-outline-dark"><i class="fa fa-comment"></i> <?php echo get_comments_number(); ?></a>

								<a href="<?php the_field('external_link'); ?>" class="btn btn-sm btn-warning bd-btn-warning" target="_blank">Get Deals <i class="fa fa-shopping-bag mr-0"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>