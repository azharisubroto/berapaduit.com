<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */


if( is_user_logged_in() ) {
	global $current_user;
	wp_get_current_user();
	$user = get_current_user_id();
}

if( ! is_user_logged_in() ) {
	$logbutton = '<a href="" class="btn-ghost" data-toggle="modal" data-target=".bd-login-modal"></a>';
} else {
	$logbutton = '';
}

bd_set_image( get_the_ID() );
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="card bd-card rb-0">
		<div class="card-body">
			<div class="row">
				<div class="col-md-3">
					<div class="item-thumb">
						<?php 
							if( has_post_thumbnail() ) {
								the_post_thumbnail('medium');
							} else {
								$image = get_post_meta( get_the_ID(), 'external_image', true );
								if( $image ) {
									echo '<img src="'.$image.'" alt="'.get_the_title().'" />';
								} else {
									echo '<img src="'.get_template_directory_uri().'/img/no-image.jpg" alt="'.get_the_title().'" />';
								}							
							}
						 ?>
					</div>
				</div>
				<div class="col-md-9 mt-3 mt-3 mt-sm-0">
					<div class="row mb-2">
						<div class="col-md-6 col-6">
							<div class="deal-vote">
								<?php echo getPostLikeLink( get_the_ID() ); echo $logbutton; ?>
							</div>
						</div>
						<div class="col-md-6 col-6 text-right fadetext small">
							<i class="fa fa-clock-o"></i> <?php printf( _x( '%s ago', '%s = human-readable time difference', 'your-text-domain' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
						</div>
					</div>
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<div class="clearfix entry-meta mb-5 mb-md-0 pb-4 pb-md-0">
						<?php 
							if( get_field('price') ):
								echo '<strong class="text-green">Rp '; 
								echo number_format( get_field('price') );
								echo '</strong> | ';
							endif; 
						?>
						<?php the_terms( get_the_ID(), 'deal', '', ', ', '' ); ?> Deal
					</div>
					<div class="entry-footer">
						<div class="inner">
							<div class="row">
								<div class="col-md-4 col-12 text-medium">
									<div class="ava inline mr-1">
										<?php echo get_avatar( get_the_author_meta('ID') , 25 ); ?>
									</div>
									<?php the_author_posts_link(); ?>
									<?php 
										if( is_user_logged_in() && get_the_author_meta('ID') === $user ) {
											$nonce = wp_create_nonce( 'wpuf_edit' );
											$pid = get_the_ID();
											echo '&nbsp;  <a href="'.home_url().'/submit-form/?pid='.$pid.'&_wpnonce='.$nonce.'"><i class="fa fa-pencil"></i> edit</a>';
										}
									 ?>
								</div>
								<div class="col-md-8 col-12 text-right entry-footer-buttons">
									<a href="<?php the_permalink(); ?>#comments" class="btn btn-sm btn-outline-dark bd-btn-outline-dark"><i class="fa fa-comment"></i> <?php echo get_comments_number(); ?></a>

									<a href="<?php the_field('external_link'); ?>" class="btn btn-sm btn-warning bd-btn-warning" target="_blank">Get Deals <i class="fa fa-shopping-bag mr-0"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- .card-body -->
	</div> <!-- .card.bd-card -->

	<div class="card bd-card rt-0 rb-0">
		<div class="card-body pt-0"> 
			<!-- Meta -->
			<div class="single-meta mb-2">
				<span class="my-2 my-md-0">
					<i class="fa fa-pencil-square-o"></i> Edited by: <?php the_author(); ?> on <?php the_modified_date('F j, Y'); ?>
				</span>
				<span class="my-2 my-md-0">
					<i class="fa fa-tag"></i> Found <?php the_date(); ?>
				</span>
			</div> <!-- .single-meta -->

			<!-- Content -->
			<div class="entry-content mb-0">
				<?php the_content(); ?>
			</div> <!-- .entry-content -->

			<!-- Share -->
			<div class="sharer mb-3">
				<?php 
					$link =  get_permalink();
					$linkparse = urlencode($link);
				 ?>
				<ul class="ball-link inline-list">
					<li><a  onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="fb"><i class="fa fa-facebook"></i></a></li>
					<li><a onclick="window.open('https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;text=<?php echo rawurlencode(get_the_title()); ?>&amp;url=<?php the_permalink();?>','Twitter','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;text=<?php echo rawurlencode(get_the_title()); ?>&amp;url=<?php the_permalink();?>" class="twit"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div> <!-- .sharer -->
			
			<!-- Tags -->
			<div class="tags fadetext mb-3">
				<?php the_terms( get_the_ID(), 'group', 'Group items: ', '' ); ?>
			</div> <!-- .tags -->

			<!-- entry-footer -->
			<div class="entry-single-footer">

				<?php comments_popup_link( '<i class="fa fa-comment"></i> New Comment', '<i class="fa fa-comment"></i> New Comment', '<i class="fa fa-comment"></i> New Comment', 'comments-link', 'Comments are off for this post'); ?>

				<?php if( is_user_logged_in() ) { ?>
					<?php the_favorites_button( get_the_ID() ); ?>

					<?php echo getPostExpires( get_the_ID() ); ?>

					<a href="#" post-id="<?php echo $post->ID; ?>" class="report-post-link"><i class="fa fa-exclamation-circle"></i>Report Thread</a>

					<span class="savepost"><?php echo do_shortcode( '[simplicity-save-for-later]' ); ?></span>

					<a href="#"><i class="fa fa-thumb-tack"></i>Embed</a>
				<?php } else { ?>
					<a href="#"  data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-eye"></i> Subscribe</a>

					<a href="#"  data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-clock-o"></i> Expired?</a>

					<a href="#"  data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-exclamation-circle"></i>Report Thread</a>

					<a href="#"  data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-bookmark"></i> Save for Later</a>

					<a href="#"  data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-thumb-tack"></i>Embed</a>
				<?php }?>
			</div>
		</div> <!-- .card-body -->
	</div>	<!-- .card bd-card -->

	<div class="modal fade" id="commentPermalink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Copy Permalink</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Permalink:</label>
							<input type="text" class="form-control" id="permalinkinput">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn btn-primary" id="copier">Copy</a>
				</div>
			</div>
		</div>
	</div>
</article><!-- #post-## -->

<div class="expired-notif alert alert-success" role="alert">
	<i class="fa fa-check"></i> Your report for an expiry has been sent
</div>
