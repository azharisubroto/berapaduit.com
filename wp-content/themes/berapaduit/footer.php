<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
?>

<div class="footer-wrapper mt-3">
	<?php //if( is_home() OR is_archive() ): ?>
	<!-- Footer NAv -->
	<div class="footer-nav">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-3">
					<a href="#" class="btn btn-light smoothtop"><i class="fa fa-caret-up"></i> <span class="netralclr">Scroll to top</span></a>
				</div>
				<div class="col-md-6 col-6">
					<?php if( is_home() OR is_search() OR is_archive() OR is_page_template( 'page-templates/current-user-deals.php' ) ): bd_pagination(); endif; ?>
				</div>
				<div class="col-md-3 col-3 text-right">
					<a href="#" class="btn btn-light foogle"><span class="netralclr">Show Footer</span> <i class="fa fa-caret-down mr-0 ml-0 ml-md-2"></i></a>
				</div>
			</div>
		</div>
	</div>
	<?php //endif; ?>

	<!-- Footer Big -->
	<div class="footer-main wrapper pb-3">
		<div class="container">
			<div class="row pb-3">
				<div class="col-md-6">
					<div class="mb-3">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
					</div>
					<div class="mb-3">
						<h5 class="largest">The World Largest Deal Community</h5>
					</div>
					<div class="mb-3 fadetext">
						BerapaDuit.com is a community for deal seeker. Find and share the best deals, 
						promotional codes and vouchers from on and off the web.
					</div>
					<!-- <div class="mb-3">
						<h5 class="largest">Events</h5>
						<a href="#">Black Friday 2017</a>
					</div> -->
					<div class="mb-3 downloadapp">
						<h5 class="largest">Get The App</h5>
						<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/appstore.png" alt=""></a>
						<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/gplay.png" alt=""></a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="statistic mb-3">
						<div class="row">
							<div class="col-md-12">
								<h5 class="largest">Statistic</h5>
							</div>
							<div class="col-md-6 col-6">
								<div class="mb-2">
									<?php 
										$result = count_users();
									 ?>
									<span><i class="fa fa-user"></i> <?php echo $result['total_users']?></span> <span class="fadetext">Users</span>
								</div>

								<div class="mb-2">
									<?php $count_pages = wp_count_posts('post'); ?>
									<span><i class="fa fa-handshake-o"></i> <?php echo $count_pages->publish; ?></span> <span class="fadetext">Deals</span>
								</div>

								<div class="mb-2">
									<?php $comments_count = wp_count_comments(); ?>
									<span><i class="fa fa-user"></i> <?php echo $comments_count->approved; ?></span> <span class="fadetext">Comments</span>
								</div>
							</div>
							<div class="col-md-6 col-6">
								<div class="mb-2">
									<?php $count = fbLikeCount('berapaduitid','1587377004678838', '97f7f308f8f45839aeee58d9ee657257'); ?>
									<a href="https://www.facebook.com/berapaduitid/">
										<span><i class="fa fa-user"></i> <?php echo $count; ?></span> <span class="fadetext">Facebook Fans</span>
									</a>
								</div>
							</div>
						</div>
					</div>

					<div class="question mb-3">
						<h5 class="largest">Questions</h5>
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'footer',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'inline-list',
								'fallback_cb'     => '',
								'menu_id'         => 'footer-menu',
							)
						); ?>
					</div>

					<div class="follow">
						<h5 class="largest">Follow Us</h5>
						<ul class="inline-list ball-link">
							<li><a href="https://www.facebook.com/berapaduitid/"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/berapaduit_id"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://instagram.com/berapaduit.id"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row mt-3 pb-3">
				<div class="col-md-12 text-center fadetext">
					Copyright &copy; 2017 Berapaduit. All rights reserved. Pepper Deals Ltd. Registered in England and Wales. Company number - 9729292. Unit 2, 1-6 Bateman's Row London EC2A3HH
				</div>
			</div>
		</div>
	</div> <!-- .footer-main -->

</div> <!-- .footer-wrapper -->


<?php wp_footer(); ?>

</body>

</html>