<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! is_active_sidebar( 'right-sidebar' ) ) {
	return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( 'both' === $sidebar_pos ) : ?>
<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">
	<?php else : ?>
<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">
	<?php endif; ?>

<aside class="card bd-card mb-3">
	<div class="card-body pt-3">
		<ul class="inline-list equalwidth">
			<li class="text-left">Hottest</li>
			<li class="text-center">
				<div class="fakebox">
					<select id="hotpostselect" class="nostyle">
						<option value="today">Today</option>
						<option value="week">Week</option>
					</select>
				</div>
			</li>
			<!-- <li class="text-right">
				<a href="#"><i class="fa fa-gear"></i></a>
			</li> -->
		</ul>
		<div class="clearfix"></div>
		<?php
			// The Query
			$today = getdate();
			$args = array(
			    'posts_per_page' => 16,          
			    'ignore_sticky_posts' => 1,  
			    'orderby' => 'comment_count',
			    'date_query' => array(
		            array(
		                'year'  => $today['year'],
		                'month' => $today['mon'],
		                'day'   => $today['mday'],
		            ),
		        ),        
			);
			$the_query = new WP_Query( $args );

			// The Loop
			echo '<div class="no-list-style ml-0 pl-0 mt-3 hotpost is_today">';
			if ( $the_query->have_posts() ) {
				
				while ( $the_query->have_posts() ) { $the_query->the_post(); 
			?>
				<div class="small hotitem">
					<div class="inner">
						<div class="row">
							<div class="col-md-4">
								<a href="<?php the_permalink(); ?>" class="netralclr small"><?php the_post_thumbnail('thumbnail');?></a>
							</div>
							
							<div class="col-md-8">
								<div class="netralclr"><?php the_terms( get_the_ID(), 'deal', '', ', ', '' ); ?></div>
								<div class="clearfix votewid">
									<?php 
										$likes = get_post_meta( get_the_ID(), "_post_like_count", true );
										if ($likes == 0) {
											$liked = '0';
										} else {
											$liked = $likes;
										}
										echo $liked;
									 ?> <i class="fa fa-flag"></i>
								</div>
								<a href="<?php the_permalink(); ?>" class="netralclr small"><?php the_title(); ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php }
				/* Restore original Post Data */
				wp_reset_postdata();
			} else { ?>
				<div class="small hotitem">
					<div class="inner">
						<div class="row">
							<div class="col-md-12">
								There's no post today
							</div>
						</div>
					</div>
				</div>				
		<?php 
			}
			echo '</div>';
		?>

		<?php
			// The Query
			$today = getdate();
			$args = array(
			    'posts_per_page' => 16,          
			    'ignore_sticky_posts' => 1,
			    'orderby' => 'comment_count',
			    'date_query'     => array(
                    array(
                    'after' => '1 week ago'
                    )
                )      
			);
			$the_query = new WP_Query( $args );

			// The Loop
			echo '<div class="no-list-style ml-0 pl-0 mt-3 hotpost is_week">';
			if ( $the_query->have_posts() ) {
				
				while ( $the_query->have_posts() ) { $the_query->the_post(); 
			?>
				<div class="small hotitem">
					<div class="inner">
						<div class="row">
							<div class="col-md-4">
								<a href="<?php the_permalink(); ?>" class="netralclr small"><?php the_post_thumbnail('thumbnail');?></a>
							</div>
							
							<div class="col-md-8">
								<div class="netralclr"><?php the_terms( get_the_ID(), 'deal', '', ', ', '' ); ?></div>
								<div class="clearfix votewid">
									<?php if(function_exists('up_down_post_votes')) { up_down_post_votes( get_the_ID(), false ); } ?> <i class="fa fa-flag"></i>
								</div>
								<a href="<?php the_permalink(); ?>" class="netralclr small"><?php the_title(); ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php }
				/* Restore original Post Data */
				wp_reset_postdata();
			} else { ?>
				<div class="small hotitem">
					<div class="inner">
						<div class="row">
							<div class="col-md-12">
								There's no post today
							</div>
						</div>
					</div>
				</div>				
		<?php 
			}
			echo '</div>';
		?>


	</div>
</aside>
<?php dynamic_sidebar( 'right-sidebar' ); ?>

</div><!-- #secondary -->
