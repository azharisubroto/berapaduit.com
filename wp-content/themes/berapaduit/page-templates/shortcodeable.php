<?php 
/**
 * Template Name: Shortcodeable Page
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */
get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
$currentlayout = bd_layout();
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="wrapper-index">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main <?php echo esc_attr( $currentlayout ); ?>" id="main">
				
				<?php 
					if( have_posts() ):
						while( have_posts() ): the_post();
							the_content();
						endwhile;
					endif;
				 ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
