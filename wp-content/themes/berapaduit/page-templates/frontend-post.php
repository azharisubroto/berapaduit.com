<?php
/**
 * Template Name: Submission
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<?php if(is_user_logged_in()): ?>

				<div
					class="<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-6<?php else : ?>col-md-12<?php endif; ?> content-area"
					id="primary">

					<main class="site-main" id="main" role="main">

						<div class="card bd-card">
							<div class="card-body">
								<?php 
									if( isset( $_GET["pid"] ) && isset( $_GET["_wpnonce"] )):
								 ?>
									<h1 class="entry-title">
										Edit Post
									</h1>

									<?php echo do_shortcode( '[wpuf_edit]' ); ?>
								<?php else: ?>
									<h1 class="entry-title">
										SUBMIT DEAL
									</h1>

									<?php echo do_shortcode( '[wpuf_form id="59"]' ); ?>
								<?php endif; ?>
							</div>
						</div>

					</main><!-- #main -->

				</div><!-- #primary -->
				
				<?php if( !isset( $_GET["pid"] ) && !isset( $_GET["_wpnonce"] )): ?>
				<div class="col-md-6">
					<div id="stickysidebar">
						<h4>Preview</h4>
						<div class="card bd-card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-3">
										<div>
											<ul class="img-preview">
												<li><img src="http://via.placeholder.com/92" alt="preview"></li>
											</ul>
										</div>
									</div>
									<div class="col-md-9">
										<div class="row mb-2">
											<div class="col-md-12">
												<div class="updown-vote-box updown-post"><div><img class="updown-button updown-up-button" vote-direction="1" src="//localhost:3000/berapaduit/wp-content/plugins/updownupdown-postcomment-voting/images/arrow-up-on.png"></div><div class="updown-total-count updown-count-sign updown-pos-count">New</div><div><img class="updown-button updown-down-button" vote-direction="-1" src="//localhost:3000/berapaduit/wp-content/plugins/updownupdown-postcomment-voting/images/arrow-down.png"></div><div class="updown-label" style="display: none;"></div></div>					
											</div>
										</div>
										<h2 class="entry-title pre-title">Your Deal Title</h2>
										<div class="entry-content">
											<p>Deal description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi rem, pariatur...</p>
										</div>
										<div class="entry-footer">
											<div class="inner">
												<div class="row">
													<div class="col-md-6">
														<?php
															global $current_user;
															get_currentuserinfo();
															$user = get_current_user_id();
														 ?>
														<div class="ava inline mr-1">
															<?php echo get_avatar( $current_user->ID , 25 ); ?>
														</div>
														<span class="text-medium"><?php echo $current_user->display_name; ?></span>							
													</div>
													<div class="col-md-6 text-right">
														<a href="javascript:;" class="btn btn-primary btn-sm">View Deal</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- .col-md-6 -->
				<?php endif; ?>

			<?php else: ?>

				<div class="col-md-12">
					<div class="card bd-card">
						<div class="card-body pt-5 pb-5 text-center text-danger">
							You don't have access to this page, please login or register first
						</div>
					</div>
				</div>

			<?php endif; ?>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
