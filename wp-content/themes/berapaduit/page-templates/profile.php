<?php
/**
 * Template Name: Profile
 *
 * Template for displaying users's page
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );

// Get User Info
global $current_user;
get_currentuserinfo();
$user = get_current_user_id();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="container">
		<div class="row sg">

			<!-- oooooooooooooooooooooooooooooooooooooooooooooooooooo
			USER STATISTIC
			ooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
			<div class="col-md-3">
				<?php bd_user_sidebar( $user ); ?>
			</div>

			<!-- oooooooooooooooooooooooooooooooooooooooooooooooooooo
			ACTIVITY
			ooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
			<div class="col-md-9">
				<ul class="nav nav-tabs nav-fill activitytab" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active pt-3 pb-3" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-briefcase"></i> Activity</a>
					</li>
					<li class="nav-item">
						<a class="nav-link pt-3 pb-3" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-star"></i> Badges</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane active card" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="card-body">
							<?php bd_user_acticity( $user ); ?>
						</div>
					</div>
					<div class="tab-pane card" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="card-body">
							<div class="pt-5 pr-3 pb-5 pl-5q text-center">
								<div class="inline">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sad.png" alt="">
								</div>
								<div class="ml-3 inline text-left">
									<strong>User's Badge is not ready for now</strong><br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
