<?php
/**
 * Template Name: Edit Profile
 *
 * Allow users to update their profiles from Frontend.
 *
 */

/* Get user info. */
global $current_user, $wp_roles;
//get_currentuserinfo(); //deprecated since 3.1

/* Load the registration file. */
//require_once( ABSPATH . WPINC . '/registration.php' ); //deprecated since 3.1
$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] )
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
        else
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
    }

    /* Update user information. */
    if ( !empty( $_POST['url'] ) )
        wp_update_user( array( 'ID' => $current_user->ID, 'user_url' => esc_url( $_POST['url'] ) ) );
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
            $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
        elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
            $error[] = __('This email is already used by another user.  try a different one.', 'profile');
        else{
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first-name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
    if ( !empty( $_POST['last-name'] ) )
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );

    /* Redirect so the page will show updated info.*/
  /*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        wp_redirect( get_permalink() );
        exit;
    }
} 
get_header();
?>

<div class="wrapper" id="full-width-page-wrapper">
    <div class="container">
        <div class="row sg">
            <!-- oooooooooooooooooooooooooooooooooooooooooooooooooooo
            USER STATISTIC
            ooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
            <div class="col-md-3">
                <?php bd_user_sidebar( $current_user->ID ); ?>
            </div>

            <!-- oooooooooooooooooooooooooooooooooooooooooooooooooooo
            USER STATISTIC
            ooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
            <div class="col-md-9">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div id="post-<?php the_ID(); ?>">
                        <div class="entry-content entry card bd-card">
                            <div class="card-body">
                                <h2 class="entry-title mb-3"><?php the_title(); ?></h2>
                                <hr>
                                <?php the_content(); ?>
                                <?php if ( !is_user_logged_in() ) : ?>
                                        <p class="warning">
                                            <?php _e('You must be logged in to edit your profile.', 'profile'); ?>
                                        </p><!-- .warning -->
                                <?php else : ?>
                                    <?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
                                    <form method="post" id="adduser" class="wpua-edit" action="" method="post" enctype="multipart/form-data">
                                        <?php 
                                            //action hook for plugin and extra fields
                                            do_action('edit_user_profile',$current_user); 
                                        ?>
                                        <p class="form-username form-group">
                                            <label for="first-name"><?php _e('First Name', 'profile'); ?></label>
                                            <input class="text-input form-control" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                                        </p><!-- .form-username -->
                                        <p class="form-username form-group">
                                            <label for="last-name"><?php _e('Last Name', 'profile'); ?></label>
                                            <input class="text-input form-control" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                                        </p><!-- .form-username -->
                                        <p class="form-email form-group">
                                            <label for="email"><?php _e('E-mail *', 'profile'); ?></label>
                                            <input class="text-input form-control" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                                        </p><!-- .form-email -->
                                        <p class="form-url">
                                            <label for="url"><?php _e('Website', 'profile'); ?></label>
                                            <input class="text-input form-control" name="url" type="text" id="url" value="<?php the_author_meta( 'user_url', $current_user->ID ); ?>" />
                                        </p><!-- .form-url -->
                                        <p class="form-passwor form-groupd">
                                            <label for="pass1"><?php _e('Password *', 'profile'); ?> </label>
                                            <input class="text-input form-control" name="pass1" type="password" id="pass1" />
                                        </p><!-- .form-password -->
                                        <p class="form-password form-group">
                                            <label for="pass2"><?php _e('Repeat Password *', 'profile'); ?></label>
                                            <input class="text-input form-control" name="pass2" type="password" id="pass2" />
                                        </p><!-- .form-password -->
                                        <?php /*
                                        <p class="form-textarea form-group">
                                            <?php 
                                                $bio = get_the_author_meta( 'description', $current_user->ID );
                                             ?>
                                            <label for="description"><?php _e('Biographical Information', 'profile') ?></label>
                                            <textarea name="description" id="description" rows="3" cols="50" class="form-control"><?php echo wp_strip_all_tags( $bio ); ?></textarea>
                                        </p><!-- .form-textarea --> */ ?>

                                        <p class="form-submit">
                                            <?php echo $referer; ?>
                                            <input name="updateuser" type="submit" id="updateuser" class="submit button btn btn-primary btn-block btn-lg" value="<?php _e('Update', 'profile'); ?>" />
                                            <?php wp_nonce_field( 'update-user' ) ?>
                                            <input name="action" type="hidden" id="action" value="update-user" />
                                        </p><!-- .form-submit -->
                                    </form><!-- #adduser -->
                                <?php endif; ?>
                            </div><!-- .card-body -->
                        </div><!-- .entry-content -->
                    </div><!-- .hentry .post -->
                    <?php endwhile; ?>
                <?php else: ?>
                    <p class="no-data">
                        <?php _e('Sorry, no page matched your criteria.', 'profile'); ?>
                    </p><!-- .no-data -->
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>