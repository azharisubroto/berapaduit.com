<?php 
/**
 * Template Name: User's Deals
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */
get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="wrapper-index">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">
				<?php 
					if ( is_user_logged_in() ):
						global $current_user;
						wp_get_current_user();
						if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
						$author_query = array('author' => $current_user->ID, 'paged' =>  $paged, 'post_status' => array('draft', 'publish', 'pending'));
						$author_posts = new WP_Query($author_query);
				 ?>

				<?php if ( $author_posts->have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php while ( $author_posts->have_posts() ) : $author_posts->the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', 'item' );
						?>

					<?php endwhile; ?>
					<?php bd_pagination_2( $author_posts->max_num_pages ); ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>
				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
