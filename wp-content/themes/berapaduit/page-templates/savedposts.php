<?php 
/**
 * Template Name: User Saved Posts
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */
get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

//if( ! is_user_logged_in() ) wp_die('you are not logged in');

?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="wrapper-index">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">
				<?php 
					// Saved objects
					$matches = array();
					if ( is_user_logged_in() ) {
						$matches = get_user_meta( get_current_user_id(), 'rs_saved_for_later', true );
					}

					if( !empty( $matches ) ){

						if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }

						$author_query = array(
							'posts_per_page' => -1,
							'post__in'       => $matches,
							'paged'			 => $paged
						);

						$author_posts = new WP_Query($author_query);
				 	
				 		if ( $author_posts->have_posts() ) {
				 			while ( $author_posts->have_posts() ) : $author_posts->the_post(); 
				 				get_template_part( 'loop-templates/content', 'item' );
							endwhile; 
						bd_pagination_2( $author_posts->max_num_pages ); 
						} else {
							get_template_part( 'loop-templates/content', 'none' ); 
						}
					}
					?>

			</main><!-- #main -->

			<!-- The pagination component -->
			

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
