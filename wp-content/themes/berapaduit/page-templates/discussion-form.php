<?php
/**
 * Template Name: Discussion Submission
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<?php if(is_user_logged_in()): ?>

				<div
					class="<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-12<?php else : ?>col-md-12<?php endif; ?> content-area"
					id="primary">

					<main class="site-main" id="main" role="main">

						<div class="card bd-card mt-3">
							<div class="card-body">
								<h1 class="entry-title">
									DISCUSSION FORM
								</h1>

								<?php echo do_shortcode( '[wpuf_form id="166"]' ); ?>
							</div>
						</div>

					</main><!-- #main -->

				</div><!-- #primary -->

			<?php else: ?>

				<div class="col-md-12">
					<div class="card bd-card">
						<div class="card-body pt-5 pb-5 text-center text-danger">
							You don't have access to this page, please login or register first
						</div>
					</div>
				</div>

			<?php endif; ?>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
