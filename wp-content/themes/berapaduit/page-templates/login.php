<?php
/**
 * Template Name: Login Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="text-center pt-5 pb-5">
					<img src="<?php echo get_template_directory_uri(); ?>/img/feed.png" alt="">
					<h3 class="bold">Connect your social account for <br> easy sign up and fast login !</h3>
					<h5 class="fadetext">Lorem ipsum dolar set amet</h5>

					<div class="clearfix"></div>

					<div class="text-enter sign-button-group mt-3">
						<?php do_action( 'wordpress_social_login' ); ?> 
						<!-- <a href="#" class="btn btn-block btn-lg fb"><i class="fa fa-facebook"></i> Sign Up with Facebook</a>
						<a href="#" class="btn btn-block btn-lg google"><i class="fa fa-google"></i> Sign Up with Google</a> -->
						<a href="#" class="btn btn-block btn-lg btn-outline-primary" id="show_login">Already have an Account?</a>
					</div>
				</div>
			</div>
			<div class="col-md-5 signup-form-side">
				<div class="pt-5 pl-5 pr-5">
					<div class="signup">
						<h3>Sign up With Email</h3>
						<hr>
						<?php echo do_shortcode( '[RM_Form id="2"]' ); ?>
					</div>
					
					<div class="signin">
						<?php echo do_shortcode( '[RM_Login]' ); ?>
					</div>

					<!-- <form action="" class="hide"> 
						<label for="email">Email</label>
						<input class="form-control mb-3" type="email" placeholder="name@domain.com">

						<label for="username">Username</label>
						<input class="form-control mb-3" type="text" placeholder="username">

						<label for="password">Password</label>
						<input class="form-control" type="password" placeholder="password">

						<div class="subscribe mt-3 mb-3">
							<div class="row">
								<div class="col-md-1"><input type="checkbox" name="daily"></div>
								<div class="col-md-11">I would like to subscribe to the daily newsletter</div>
							</div>
							<div class="clearfix"></div>
							<div class="row">
								<div class="col-md-1"><input type="checkbox" name="weekly"></div>
								<div class="col-md-11">I would like to subscribe to the weekly community newsletter</div>
							</div>
						</div>

						<button class="btn btn-lsg btn-block btn-primary">Create New Account</button>
					</form> -->
					
				</div>
			</div>
		</div>
	</div>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
