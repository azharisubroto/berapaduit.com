<?php
if( ! function_exists('bd_notifications_feed') ):
	function bd_notifications_feed() {
		wp_get_current_user();
		$user = get_current_user_id();
		$text = $icon = '';

		/* get post ids by current user ID */
			if ( is_user_logged_in() ):
				global $current_user;
				wp_get_current_user();
				$author_query = array(
					'post_type' => array('post', 'discussion'),
					'author' => $current_user->ID, 
					'posts_per_page' => -1
				);
				$author_posts = new WP_Query($author_query);

				$postids = array();
				if ( $author_posts->have_posts() ) {
					while ( $author_posts->have_posts() ) : $author_posts->the_post(); 
						$postids[] = get_the_ID();
					endwhile; wp_reset_postdata();
				}

				/* Get user subscribed */
				if( function_exists('get_user_favorites') ) {
					$userfavposts = get_user_favorites( $current_user->ID );

					foreach( $userfavposts as $id ) {
						$postids[] = $id;
					}
				}
			endif; 

		// The Query
		/*echo '<pre>';
		print_r($postids);
		echo '</pre>';*/
		
		if( !empty( $postids ) ){

			$args = array(
				'post__in' => $postids
			);
			$comments = get_comments( $args );

			// Comment Loop
			if ( $comments ) {
				foreach ( $comments as $comment ) {
					$userid = $comment->user_id;
					$post_author_id = get_post_field( 'post_author', $comment->comment_post_ID );
					if( $userid != $current_user->ID ):
						if( function_exists('get_user_favorites') && in_array( $comment->comment_post_ID, $userfavposts ) && $userid !== $current_user->ID ) {
							$text = ' a post you followed ';
							$icon = '<i class="fa fa-eye"></i>';
						} elseif( $post_author_id == $current_user->ID ) {
							$text = ' your post: ';
							$icon = '<i class="fa fa-comment"></i>';
						}
						echo '<a class="dropdown-item notif-item" href="'.get_permalink( $comment->comment_post_ID ).'#comment-'.$comment->comment_ID.'"><div class="notif-icon">'.$icon.' </div>';
						echo '<div class="notif-content">';
						echo '<strong>'.$comment->comment_author.'</strong>';
						echo ' Commented on '.$text;
						echo '<strong>'.get_the_title( $comment->comment_post_ID ).'</strong>';
						echo '<br>on <span class="fadetext text-small">'.time_elapsed_string( $comment->comment_date_gmt ).'</span>';
						echo '</div>';
						echo '</a>';
					endif;
				}
			} 
		} else {
			echo '<div class="dropdown-item">Nothing to view</div>';
		}
	}
endif;