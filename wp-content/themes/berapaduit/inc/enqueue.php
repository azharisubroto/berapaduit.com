<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), filemtime(get_template_directory().'/css/theme.min.css'), false );
		
		wp_deregister_script('jquery');
			wp_register_script('jquery', (get_stylesheet_directory_uri() . '/js/jquery-3.2.1.min.js'), false, '3.2.1');
			wp_enqueue_script( 'jquery' );

		//wp_register_script('jquery-slim', (get_template_directory_uri() . '/js/jquery.slim.min.js'), true, '3.2.1');
		//wp_enqueue_script( 'jquery-slim' );
		
		wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), true);
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $the_theme->get( 'Version' ), true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		$status = '';
		if( is_user_logged_in() ) {
			$status = 'loggedin';
		} else {
			$status = 'loggedout';
		}
		wp_localize_script( 'understrap-scripts', 'berapaduit', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'siteurl' => home_url(),
			'loadmore' => esc_html__( 'LOAD MORE', 'jt-wishbone' ),
			'loading' => esc_html__( 'LOADING', 'jt-wishbone' ),
			'caughtup' => esc_html__( 'ALL CAUGHT UP', 'jt-wishbone' ),
			'readmore' => esc_html__( 'Read More', 'jt-wishbone' ),
			'less' => esc_html__( 'Less', 'jt-wishbone' ),
			'nonce' => wp_create_nonce( 'ajax-nonce' ),
			'userstatus' => $status,
		) );
	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
