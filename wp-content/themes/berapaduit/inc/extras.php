<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package understrap
 */

function remove_head_scripts() { 
   remove_action('wp_head', 'wp_print_scripts'); 
   remove_action('wp_head', 'wp_print_head_scripts', 9); 
   remove_action('wp_head', 'wp_enqueue_scripts', 1);
 
   add_action('wp_footer', 'wp_print_scripts', 5);
   add_action('wp_footer', 'wp_enqueue_scripts', 5);
   add_action('wp_footer', 'wp_print_head_scripts', 5); 
} 
//add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
 
// END Custom Scripting to Move JavaScript

if ( ! function_exists( 'understrap_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function understrap_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}
add_filter( 'body_class', 'understrap_body_classes' );

// Set post thumbnail from external URL
if( ! function_exists('bd_set_image') ):
function bd_set_image( $post_id ) {

	// Add Featured Image to Post
	if( has_post_thumbnail( $post_id ) ) return false;
	$image_url        = (string) get_post_meta( get_the_ID(), 'external_image', true ); 

	if( $image_url ):
		$parts = parse_url($image_url);  
		$file_name = basename($parts['path']);  
		$image_name = $file_name;	
		$upload_dir       = wp_upload_dir(); // Set upload folder
		$image_data       = file_get_contents($image_url); // Get image data
		$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
		$filename         = basename( $unique_file_name ); // Create image file name

		// Check folder permission and define file location
		if( wp_mkdir_p( $upload_dir['path'] ) ) {
		    $file = $upload_dir['path'] . '/' . $filename;
		} else {
		    $file = $upload_dir['basedir'] . '/' . $filename;
		}

		// Create the image  file on the server
		file_put_contents( $file, $image_data );

		// Check image file type
		$wp_filetype = wp_check_filetype( $filename, null );

		// Set attachment data
		$attachment = array(
		    'post_mime_type' => $wp_filetype['type'],
		    'post_title'     => sanitize_file_name( $filename ),
		    'post_content'   => '',
		    'post_status'    => 'inherit'
		);

		// Create the attachment
		$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

		// Include image.php
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

		// Assign metadata to attachment
		wp_update_attachment_metadata( $attach_id, $attach_data );

		// And finally assign featured image to post
		set_post_thumbnail( $post_id, $attach_id );
	endif;	
}
endif;
//add_action( 'save_post', 'bd_set_image' );
//add_action( 'wpuf_add_post_after_insert', 'bd_set_image' );
//add_action( 'wpuf_edit_post_after_update', 'bd_set_image' );

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'adjust_body_class' );

if ( ! function_exists( 'adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'change_logo_class' );

if ( ! function_exists( 'change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"' , $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */
if ( ! function_exists( 'understrap_post_nav' ) ) :

	function understrap_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
			<nav class="container navigation post-navigation">
				<h2 class="sr-only"><?php _e( 'Post navigation', 'understrap' ); ?></h2>
				<div class="row nav-links justify-content-between">
					<?php

						if ( get_previous_post_link() ) {
							previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'understrap' ) );
						}
						if ( get_next_post_link() ) {
							next_post_link( '<span class="nav-next">%link</span>',     _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'understrap' ) );
						}
					?>
				</div><!-- .nav-links -->
			</nav><!-- .navigation -->

		<?php
	}
endif;

// Custom Excerpt 
if( ! function_exists('bd_excerpt') ) :
	function bd_excerpt($limit) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'<a href="'.get_permalink().'" class="netralclr"><strong>...Read More</strong></a>';
		} else {
		$excerpt = implode(" ",$excerpt);
		} 
		$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
		return $excerpt;
	}
endif;

add_filter('show_admin_bar', '__return_false');

if( ! function_exists('bd_layout')  ):
	function bd_layout() {
		if(!empty($_COOKIE['loopview'])){
			if ($_COOKIE['loopview'] == "default") {
				$containerclass = 'default';
			} else {
				$containerclass = 'grid';
			} 
		} else {
			$containerclass = 'default';
		}
		return $containerclass;
	}
endif;

// Logout link
add_filter( 'wp_nav_menu_items', 'bd_loginout_menu_link', 10, 2 );

// Show user logout link
function bd_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'profile') {
      if (is_user_logged_in()) {
         $items .= '<li class="menu-item nav-item"><a href="'. wp_logout_url( home_url() ) .'" class="nav-link text-center"><i class="fa fa-sign-out"></i> '. __("Log Out") .'</a></li>';
      } else {
         $items .= '<li class="menu-item nav-item"><a href="'. wp_login_url(get_permalink()) .'" class="nav-link text-right"><i class="fa fa-sign-in"></i> '. __("Log In") .'</a></li>';
      }
   }
   return $items;
}

// Return only posts that have comments
function my_has_comments_filter( $where ) {
    $where .= ' AND comment_count > 0 ';
    return $where;
}

if( ! function_exists('bd_loginform') ):
	function bd_loginform() {
?>
	<div class="row">
		<div class="col-md-7">
			<div class="text-center pt-5 pb-5">
				<img src="<?php echo get_template_directory_uri(); ?>/img/feed.png" alt="">
				<h3 class="bold">Connect your social account for <br> easy sign up and fast login !</h3>
				<h5 class="fadetext">Lorem ipsum dolar set amet</h5>

				<div class="clearfix"></div>

				<div class="text-enter sign-button-group mt-3">
					<?php do_action( 'wordpress_social_login' ); ?> 
					<!-- <a href="#" class="btn btn-block btn-lg fb"><i class="fa fa-facebook"></i> Sign Up with Facebook</a>
					<a href="#" class="btn btn-block btn-lg google"><i class="fa fa-google"></i> Sign Up with Google</a> -->
					<a href="#" class="btn btn-block btn-lg btn-outline-primary" id="show_login">Already have an Account?</a>
				</div>
			</div>
		</div>
		<div class="col-md-5 signup-form-side">
			<div class="pt-5 pl-5 pr-5">
				<div class="signup">
					<h3>Sign up With Email</h3>
					<hr>
					<?php echo do_shortcode( '[RM_Form id="2"]' ); ?>
				</div>
				
				<div class="signin">
					<h3>Sign In</h3>
					<hr>
					<?php echo do_shortcode( '[RM_Login]' ); ?>
				</div>

				<!-- <form action="" class="hide"> 
					<label for="email">Email</label>
					<input class="form-control mb-3" type="email" placeholder="name@domain.com">

					<label for="username">Username</label>
					<input class="form-control mb-3" type="text" placeholder="username">

					<label for="password">Password</label>
					<input class="form-control" type="password" placeholder="password">

					<div class="subscribe mt-3 mb-3">
						<div class="row">
							<div class="col-md-1"><input type="checkbox" name="daily"></div>
							<div class="col-md-11">I would like to subscribe to the daily newsletter</div>
						</div>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-1"><input type="checkbox" name="weekly"></div>
							<div class="col-md-11">I would like to subscribe to the weekly community newsletter</div>
						</div>
					</div>

					<button class="btn btn-lsg btn-block btn-primary">Create New Account</button>
				</form> -->
				
			</div>
		</div>
	</div>
<?php
	}
endif;

// Shortcode for POSTS
function bd_hot_posts($atts, $content = null) {
	$daysagoattr = $orderbyattr = $discussedattr = '';
	$a = shortcode_atts( array(
        'daysago' => '',
        'orderby'  =>  '',
        'discussed'  =>  '',
    ), $atts );

	// The Query
	if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }

    $args = array(
        'posts_per_page' => get_option('posts_per_page'),       
        'ignore_sticky_posts' => 1,
        'paged' =>  $paged,
    );
	$today = getdate();

	if( isset( $a['daysago'] ) ) {
		$age =  (string)$a['daysago'];
		$daysago = $age." days ago";
		$args = array_merge( $args, array( 
				'date_query'     => array(
                    array(
                        'column' => 'post_date_gmt',
						'after'  => $daysago,
                    ),
                )
			)
		);

		$daysagoattr = 'data-daysago="'.$a['daysago'].'" ';
	}

	if( isset( $a['orderby'] ) ) {
		$args = array_merge( $args, array( "orderby" => $a['orderby'] ) );
		$orderbyattr = 'data-orderby="'.$a['orderby'].'" ';
	}

	if( isset( $a['discussed'] ) && 'yes' == $a['discussed'] ) {
		add_filter( 'posts_where', 'my_has_comments_filter' );
		$discussedattr = 'data-discussed="yes" ';
	}

	$the_query = new WP_Query( $args );
	
	$maxpages = $the_query->max_num_pages;

	if( isset( $a['discussed'] ) && 'yes' == $a['discussed'] ) {
		remove_filter( 'posts_where', 'my_has_comments_filter' );
	}

	// The Loop
	if ( $the_query->have_posts() ) {
		
		while ( $the_query->have_posts() ) { $the_query->the_post(); 
			get_template_part( 'loop-templates/content', 'item' );
		}
	?>
		<a href="#"
		class="wishbone_loadmorebutton"
		data-perpage="<?php echo esc_attr( get_option('posts_per_page') ); ?>"
		data-currentpaged="<?php echo esc_attr( $paged ); ?>"
		data-maxpages="<?php echo esc_attr( $maxpages );  ?>"
		<?php echo $orderbyattr; echo $daysagoattr; echo $discussedattr; ?>>
			<?php esc_html_e( 'LOAD MORE', 'jt-wishbone' ); ?>
		</a>
	<?php
		/* Restore original Post Data */
		wp_reset_postdata();
	} else { 
		get_template_part( 'loop-templates/content', 'none' );
	}
}
add_shortcode( 'bd_hot_posts', 'bd_hot_posts' );

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

// Modify who can read a logger.
// Modify the if part to give users access or no access to a logger.
add_filter( 'simple_history/loggers_user_can_read/can_read_single_logger', function( $user_can_read_logger, $logger_instance, $user_id ) {
	
	return true;

}, 10, 3 );

function bd_user_acticity( $user ) {
?>
	<?php 
		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }

		// Get log rows
		$args = array(
			'posts_per_page' => 999999999,
			'user' => $user,
			'loggers' => 'SimpleCommentsLogger,SimplePostLogger',
			'message' => 'user_comment_added,post_updated,post_added',
		);

		$args = apply_filters( 'simple_history/rss_feed_args', $args );

		$logQuery = new SimpleHistoryLogQuery();
		$queryResults = $logQuery->query( $args );
		$rows = $logQuery->query( $args );
		$rows = json_decode(json_encode($rows['log_rows']), True);

		$authorname = get_the_author_meta( 'first_name', $user );
		$authorlastname = get_the_author_meta( 'last_name', $user );

		
		if( array( $rows ) && !empty($rows) ):
			echo '<div class="activity-stream">';
			foreach( $rows as $row ){

				$useremail = $row['context']['_user_email'];
				$context = $row['logger'];
				if( 'page' !== $post_type && 'SimplePostLogger' == $context ) {
					$post_type = $row['context']['post_type'];
					$post_type = str_replace('post', 'deal', $post_type);
					$post_title = $row['context']['post_title'];
					$permalink = get_permalink( $row['context']['post_id'] );
					$message = ' created a '.$post_type.': <a href="'.$permalink.'">'.$post_title.'</a>';
					if( isset( $post_type ) ) {
						if( 'deal' == $post_type OR 'discussion' == $post_type ) {
			?>
							<div class="activity-item clearfix text-medium">
								<a href="<?php echo esc_url( get_author_posts_url($user) ); ?>" class="wp-user-activity user-link alignleft">
									<?php 
										echo get_avatar( $useremail, 30 ); 
									?>							
								</a>
								<a href="<?php echo esc_url( get_author_posts_url($user) ); ?>"><?php echo $authorname; ?></a> 
								<?php echo $message; ?>
								<time class="diff-time text-small float-right">
									<?php echo time_elapsed_string( $row['date'] ); ?>
								</time>
							</div>
			<?php 
						}
					}
				} elseif( 'SimpleCommentsLogger' == $context ) {
					$comment_post_title = $row['context']['comment_post_title'];
					$post_id = $row['context']['comment_post_ID'];
					$post_type = $row['context']['comment_post_type'];
					$post_type = str_replace('post', 'deal', $post_type);
					$message = ' left a comment on a '.$post_type.': <a href="'.get_permalink( $post_id ).'">'.$comment_post_title.'</a>';
			?>
					<div class="activity-item clearfix text-medium">
						<a href="<?php echo esc_url( get_author_posts_url($user) ); ?>" class="wp-user-activity user-link alignleft">
							<?php 
								echo get_avatar( $useremail, 30 ); 
							?>							
						</a>
						<a href="<?php echo esc_url( get_author_posts_url($user) ); ?>"><?php echo $authorname; ?></a> 
						<?php echo $message; ?>
						<time class="diff-time text-small float-right">
							<?php echo time_elapsed_string( $row['date'] ); ?>
						</time>
					</div>
			<?php
				}
			}
			echo '</div>';
			/*echo '<pre>';
			print_r( $rows );
			echo '</pre>';*/
		else:
	 ?>
		<div class="pt-5 pr-3 pb-5 pl-5">
			<div class="text-center">
				<div class="inline">
					<img src="<?php echo get_template_directory_uri(); ?>/img/sad.png" alt="">
				</div>
				<div class="ml-3 inline text-left">
					<strong>Hmmm</strong><br>
					It looks like you don’t have any activities yet...
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php
}

function bd_user_sidebar( $user ) {
?>
	<div class="card bd-card user-statistic">
		<div class="card-body">
			<i class="fa fa-area-chart"></i> Statistic
		</div>

		<!-- ooooooooooooo
		POSTS
		ooooooooooooooo -->
		<div class="card-body text-medium pt-0">
			
			<span class="text-gray">Posts</span>

			<!-- DEALS -->
			<div class="clearfix mt-2">
				<div class="float-left mr-2">
					<i class="fa fa-tag"></i>
				</div>
				<div class="float-left mr-3">
					<?php $user_post_count = count_user_posts( $user , 'post' ); ?>
					<?php echo $user_post_count; ?> <span class="text-small text-gray">deals</span>
				</div>
			</div> <!-- /DEALS -->

			<!-- Discussions -->
			<div class="clearfix">
				<div class="float-left mr-2">
					<i class="fa fa-comments"></i>
				</div>
				<div class="float-left mr-3">
					<?php $user_discusstion_count = count_user_posts( $user , 'discussion' ); ?>
					<?php echo $user_discusstion_count; ?> <span class="text-small text-gray">discussions</span>
				</div>
			</div> <!-- /Discussions -->

			<!-- Comments -->
			<div class="clearfix">
				<div class="float-left mr-2">
					<i class="fa fa-comment"></i>
				</div>
				<div class="float-left mr-3">
					<?php 
						$count_comment_deal = count( get_comments( array(
						    'post_type' => 'post',
						    'author__in' => array( $user )
						) ) );
					 ?>
					<?php echo $count_comment_deal; ?> <span class="text-small text-gray">comments</span>
				</div>
			</div> <!-- /Comments -->

			<!-- Discussions Comments -->
			<div class="clearfix">
				<div class="float-left mr-2">
					<i class="fa fa-commenting"></i>
				</div>
				<div class="float-left mr-3">
					<?php 
						$discusscount = count( get_comments( array(
						    'post_type' => 'discussion',
						    'author__in' => array( $user )
						) ) );
					 ?>
					<?php echo $discusscount; ?> <span class="text-small text-gray">discussion comments</span>
				</div>
			</div> <!-- /Discussions Comments -->

		</div> <!-- end POSTS .card-body -->

		<!-- ooooooooooooo
		TRENDS
		ooooooooooooooo -->
		<div class="card-body pt-0">
			
			<span class="text-gray">Trends</span>

			<!-- Hottest-->
			<div class="clearfix mt-2">
				<div class="float-left mr-2">
					<i class="fa fa-flag"></i>
				</div>
				<div class="float-left mr-3">
					0 <span class="text-small text-gray">hottest</span>
				</div>
			</div> <!-- /Hottest-->

			<!-- Average of Last Year -->
			<div class="clearfix">
				<div class="float-left mr-2">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="float-left mr-3">
					0 <span class="text-small text-gray">average of last year</span>
				</div>
			</div> <!-- /Average of Last Year -->

			<!-- hote deal percentage -->
			<div class="clearfix">
				<div class="float-left mr-2">
					<i class="fa fa-pie-chart"></i>
				</div>
				<div class="float-left mr-3">
					0 <span class="text-small text-gray">hot deal percentage</span>
				</div>
			</div> <!-- /hote deal percentage -->

		</div> <!-- END TRENDS .card-body -->

		<!-- ooooooooooooo
		COMMUNITY
		ooooooooooooooo -->
		<div class="card-body pt-0">
			
			<span class="text-gray">Community</span>

			<!-- Hottest-->
			<div class="clearfix mt-2">
				<div class="float-left mr-2">
					<i class="fa fa-thumbs-up"></i>
				</div>
				<div class="float-left mr-3">
					<?php 
						$user_likes = get_user_meta( $user, "_liked_posts");
						if ( $user_likes && count( $user_likes ) > 0 ) {
							$the_likes = $user_likes[0];
						} else {
							$the_likes = '';
						}
						
						if ( !is_array( $the_likes ) )
						$the_likes = array();
						$count = count($the_likes); $i=0;
					 ?>
					<?php echo $count; ?> <span class="text-small text-gray">likes</span>
				</div>
			</div> <!-- /Hottest-->

			<!-- Average of Last Year -->
			<div class="clearfix">
				<div class="float-left mr-2">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="float-left mr-3">
					0 <span class="text-small text-gray">followers</span>
				</div>
			</div> <!-- /Average of Last Year -->

		</div> <!-- END COMMUNITY .card-body -->

	</div>
<?php
}

/*==========================
 This snippet contains utility functions to create/update and pull data from the active user transient.
 Copy these contents to functions.php
 ===========================*/
//Update user online status
add_action('init', 'berapaduit_users_status_init');
add_action('admin_init', 'berapaduit_users_status_init');
function berapaduit_users_status_init(){
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	$user = wp_get_current_user(); //Get the current user's data
	//Update the user if they are not on the list, or if they have not been online in the last 30 seconds (15 minutes)
	if ( !isset($logged_in_users[$user->ID]['last']) || $logged_in_users[$user->ID]['last'] <= time()-30 ){
		$logged_in_users[$user->ID] = array(
			'id' => $user->ID,
			'username' => $user->user_login,
			'last' => time(),
		);
		set_transient('users_status', $logged_in_users, 30); //Set this transient to expire 15 minutes after it is created.
	}
}

//Check if a user has been online in the last 15 minutes
function berapaduit_is_user_online($id){	
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	
	return isset($logged_in_users[$id]['last']) && $logged_in_users[$id]['last'] > time()-30; //Return boolean if the user has been online in the last 30 seconds (15 minutes).
}

//Check when a user was last online.
function berapaduit_user_last_online($id){
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	
	//Determine if the user has ever been logged in (and return their last active date if so).
	if ( isset($logged_in_users[$id]['last']) ){
		return $logged_in_users[$id]['last'];
	} else {
		return false;
	}
}

/* ooooooooooooooooooooooooooooooooooooooooooooo
ADD GROUP ITEM
ooooooooooooooooooooooooooooooooooooooooooooo */
add_action( 'wp_ajax_nopriv_add-term', 'add_term' );
add_action( 'wp_ajax_add-term', 'add_term' );
function add_term() {
	$nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        die ( 'Nope!' );

	if( $_POST['group_item'] ) {
		// echo $_POST['group_item'];
		$term = $_POST['group_item'];
		$termexists = term_exists( $term, 'group' );
		if( $term !== 0 && $term !== null ) {
			$cid = wp_insert_term( $term, 'group' );

			if ( ! is_wp_error( $cid ) )
			{
			    // Get term_id, set default as 0 if not set
			    $cat_id = isset( $cid['term_id'] ) ? $cid['term_id'] : 0;
			    echo $cat_id;
			}
			else
			{
			     // Trouble in Paradise:
			     echo $cid->get_error_message();
			}
		}
	}
	exit;
}

/* ooooooooooooooooooooooooooooooooooooooooooooo
Live Search
ooooooooooooooooooooooooooooooooooooooooooooo */
// Remove pages from search
add_action('init', 'remove_pages_from_search');
function remove_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}

// Process live search
add_action( 'wp_ajax_nopriv_bd-live-search', 'bd_live_search' );
add_action( 'wp_ajax_bd-live-search', 'bd_live_search' );
function bd_live_search() {
	if( $_POST['search'] ) {
		$term = $_POST['search'];
		$query = new WP_Query( array( 'post_type' => 'post', 's' => $term, 'posts_per_page' => 5 ) );
		$post_count = $query->found_posts;

		ob_start();
		if( $query->have_posts() ) {
			while( $query->have_posts() ): $query->the_post(); ?>
				<li <?php post_class();?> >
					<a href="<?php the_permalink(); ?>" class="search-link">
						<div class="row">
							<div class="col-md-9 col-9">
								<span class="bd-deal-degree">
									<?php 
										$deg = get_post_meta( get_the_ID(), "_post_like_count", true );
										if( $deg ) {
											echo $deg; 
										} else {
											echo '0';
										}
									?>&deg;
								</span>
								<?php the_title(); ?>
								<?php 
									if( get_field('price') ):
										echo '<strong class="text-green">Rp '; 
										echo number_format( get_field('price') );
										echo '</strong>';
									endif; 
								?>
							</div>
							<div class="col-md-3 col-3">
								<div class="search-thumb">
									<?php 
										if( has_post_thumbnail() ) {
											the_post_thumbnail('small');
										} else {
											$image = get_post_meta( get_the_ID(), 'external_image', true );
											if( $image ) {
												echo '<img src="'.$image.'" alt="'.get_the_title().'" />';
											} else {
												echo '<img src="'.get_template_directory_uri().'/img/no-image.jpg" alt="'.get_the_title().'" />';
											}							
										}
									 ?>
								</div>
							</div>
						</div>
					</a>
				</li>
		<?php
			endwhile;

			if( $post_count > 5 ) {
				$url = home_url('/');
				$url = $url.'?s='.$term;
				echo '<li class="p-2"><a href="'.$url.'" class="btn btn-success btn-sm btn-block text-white">See More</a></li>';
			}
		}
		$html = ob_get_clean();
		$response = array(
	        'sucess' => true,
	        'html' => $html,
	    );

	    // generate the response
	    print json_encode($response);
	}
	exit;
}

/*==========================
 This snippet shows how to add a column to the Users admin page with each users' last active date.
 Copy these contents to functions.php
 ===========================*/
 
 //Add columns to user listings
add_filter('manage_users_columns', 'berapaduit_user_columns_head');
function berapaduit_user_columns_head($defaults){
    $defaults['status'] = 'Status';
    return $defaults;
}
add_action('manage_users_custom_column', 'berapaduit_user_columns_content', 15, 3);
function berapaduit_user_columns_content($value='', $column_name, $id){
    if ( $column_name == 'status' ){
		if ( berapaduit_is_user_online($id) ){
			return '<strong style="color: green;">Online Now</strong>';
		} else {
			return '<strong style="color: red;">Offline</strong>';
		}
	}
}

/*==========================
 This snippet shows how to add an active user count to the WordPress Dashboard.
 Copy these contents to functions.php
 ===========================*/
//Active Users Metabox
add_action('wp_dashboard_setup', 'berapaduit_activeusers_metabox');
function berapaduit_activeusers_metabox(){
	global $wp_meta_boxes;
	wp_add_dashboard_widget('berapaduit_activeusers', 'Active Users', 'dashboard_berapaduit_activeusers');
}
function dashboard_berapaduit_activeusers(){
		$user_count = count_users();
		$users_plural = ( $user_count['total_users'] == 1 )? 'User' : 'Users'; //Determine singular/plural tense
		echo '<div><a href="users.php">' . $user_count['total_users'] . ' ' . $users_plural . '</a> <small>(' . berapaduit_online_users('count') . ' currently active)</small></div>';
}
//Get a count of online users, or an array of online user IDs.
//Pass 'count' (or nothing) as the parameter to simply return a count, otherwise it will return an array of online user data.
function berapaduit_online_users($return='count'){
	$logged_in_users = get_transient('users_status');
	
	//If no users are online
	if ( empty($logged_in_users) ){
		return ( $return == 'count' )? 0 : false; //If requesting a count return 0, if requesting user data return false.
	}
	
	$user_online_count = 0;
	$online_users = array();
	foreach ( $logged_in_users as $user ){
		if ( !empty($user['username']) && isset($user['last']) && $user['last'] > time()-30 ){ //If the user has been online in the last 30 seconds, add them to the array and increase the online count.
			$online_users[] = $user;
			$user_online_count++;
		}
	}
	return ( $return == 'count' )? $user_online_count : $online_users; //Return either an integer count, or an array of all online user data.
}

function fbLikeCount($id,$appid,$appsecret){
  $json_url ='https://graph.facebook.com/'.$id.'?access_token='.$appid.'|'.$appsecret.'&fields=fan_count';
  $json = file_get_contents($json_url);
  $json_output = json_decode($json);
  //Extract the likes count from the JSON object
  if($json_output->fan_count){
    return $fan_count = $json_output->fan_count;
  }else{
    return 0;
  }
}