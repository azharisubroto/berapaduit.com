<?php 
/*-----------------------------------------------------------------------------------*/
//Vote/Likes system
/*-----------------------------------------------------------------------------------*/

/**
 * (1) Enqueue scripts for like system
 */


/**
 * (3) Save like data
 */
add_action( 'wp_ajax_nopriv_post-expires', 'post_expires' );
add_action( 'wp_ajax_post-expires', 'post_expires' );
function post_expires() {
	$nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        die ( 'Nope!' );
	
	if ( isset( $_POST['post_expires'] ) ) {
	
		$post_id = $_POST['post_id']; // post id
		$post_expires_count = get_post_meta( $post_id, "_post_expires_count", true ); // post like count
		
		if ( is_user_logged_in() ) { // user is logged in
			global $current_user;
			$user_id = $current_user->ID; // current user
			$meta_POSTS = get_user_meta( $user_id, "_liked_posts" ); // post ids from user meta
			$meta_USERS = get_post_meta( $post_id, "_user_expired" ); // user ids from post meta
			$liked_POSTS = ""; // setup array variable
			$liked_USERS = ""; // setup array variable
			
			if ( count( $meta_POSTS ) != 0 ) { // meta exists, set up values
				$liked_POSTS = $meta_POSTS[0];
			}
			
			if ( !is_array( $liked_POSTS ) ) // make array just in case
				$liked_POSTS = array();
				
			if ( count( $meta_USERS ) != 0 ) { // meta exists, set up values
				$liked_USERS = $meta_USERS[0];
			}		

			if ( !is_array( $liked_USERS ) ) // make array just in case
				$liked_USERS = array();
				
			$liked_POSTS['post-'.$post_id] = $post_id; // Add post id to user meta array
			$liked_USERS['user-'.$user_id] = $user_id; // add user id to post meta array
			$user_likes = count( $liked_POSTS ); // count user likes
	
			if ( !AlreadyReported( $post_id ) ) { // like the post
				update_post_meta( $post_id, "_user_expired", $liked_USERS ); // Add user ID to post meta
				update_post_meta( $post_id, "_post_expires_count", ++$post_expires_count ); // +1 count post meta
				update_user_meta( $user_id, "_liked_posts", $liked_POSTS ); // Add post ID to user meta
				update_user_meta( $user_id, "_user_like_count", $user_likes ); // +1 count user meta
				echo $post_expires_count; // update count on front end
				
			} else { // unlike the post
				/*$pid_key = array_search( $post_id, $liked_POSTS ); // find the key
				$uid_key = array_search( $user_id, $liked_USERS ); // find the key
				unset( $liked_POSTS[$pid_key] ); // remove from array
				unset( $liked_USERS[$uid_key] ); // remove from array
				$user_likes = count( $liked_POSTS ); // recount user likes
				update_post_meta( $post_id, "_user_expired", $liked_USERS ); // Remove user ID from post meta
				update_post_meta($post_id, "_post_expires_count", ++$post_expires_count ); // -1 count post meta
				update_user_meta( $user_id, "_liked_posts", $liked_POSTS ); // Remove post ID from user meta			
				update_user_meta( $user_id, "_user_like_count", $user_likes ); // -1 count user meta*/
				echo "already".$post_expires_count; // update count on front end
				
			}
			
		} else { // user is not logged in (anonymous)
			$ip = $_SERVER['REMOTE_ADDR']; // user IP address
			$meta_IPS = get_post_meta( $post_id, "_user_IP" ); // stored IP addresses
			$liked_IPS = ""; // set up array variable
			
			if ( count( $meta_IPS ) != 0 ) { // meta exists, set up values
				$liked_IPS = $meta_IPS[0];
			}
	
			if ( !is_array( $liked_IPS ) ) // make array just in case
				$liked_IPS = array();
				
			if ( !in_array( $ip, $liked_IPS ) ) // if IP not in array
				$liked_IPS['ip-'.$ip] = $ip; // add IP to array
			
			if ( !AlreadyReported( $post_id ) ) { // like the post
			
				update_post_meta( $post_id, "_user_IP", $liked_IPS ); // Add user IP to post meta
				update_post_meta( $post_id, "_post_expires_count", ++$post_expires_count ); // +1 count post meta
				echo $post_expires_count; // update count on front end
				
			} else { // unlike the post
			
				$ip_key = array_search( $ip, $liked_IPS ); // find the key
				unset( $liked_IPS[$ip_key] ); // remove from array
				update_post_meta( $post_id, "_user_IP", $liked_IPS ); // Remove user IP from post meta
				update_post_meta( $post_id, "_post_expires_count", --$post_expires_count ); // -1 count post meta
				echo "already".$post_expires_count; // update count on front end
				
			}
		}
	}
	
	exit;
}

/**
 * (4) Test if user already liked post
 */
function AlreadyReported( $post_id ) { // test if user liked before
	
	if ( is_user_logged_in() ) { // user is logged in
		global $current_user;
		$user_id = $current_user->ID; // current user
		$meta_USERS = get_post_meta( $post_id, "_user_expired" ); // user ids from post meta
		$liked_USERS = ""; // set up array variable
		
		if ( count( $meta_USERS ) != 0 ) { // meta exists, set up values
			$liked_USERS = $meta_USERS[0];
		}
		
		if( !is_array( $liked_USERS ) ) // make array just in case
			$liked_USERS = array();
			
		if ( in_array( $user_id, $liked_USERS ) ) { // True if User ID in array
			return true;
		}
		return false;
		
	} else { // user is anonymous, use IP address for voting
	
	$meta_IPS = get_post_meta($post_id, "_user_IP"); // get previously voted IP address
	$ip = $_SERVER["REMOTE_ADDR"]; // Retrieve current user IP
	$liked_IPS = ""; // set up array variable

	if ( count( $meta_IPS ) != 0 ) { // meta exists, set up values
		$liked_IPS = $meta_IPS[0];
	}

	if ( !is_array( $liked_IPS ) ) // make array just in case
		$liked_IPS = array();

	if ( in_array( $ip, $liked_IPS ) ) { // True is IP in array
		return true;
	}
	return false;
	}
	
}

/**
 * (5) Front end button
 */
function getPostExpires( $post_id ) {
	$theme_object = wp_get_theme();
	$themename = esc_attr( $theme_object->Name ); // the theme name
	$like_count = get_post_meta( $post_id, "_post_expires_count", true ); // get post likes
if ( ( !$like_count ) || ( $like_count && $like_count == "0" ) ) { // no votes, set up empty variable
	$likes = '0';
} elseif ( $like_count && $like_count != "0" ) { // there are votes!
	$likes = esc_attr( $like_count );
}
	
	if ( AlreadyReported( $post_id ) ) { // already liked, set up unlike addon
		$output .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="you have reported this thread as expired" class="text-primary"><span class="unliker"><i class="fa fa-no-alt"></i></span><span class="like prevliked"><i class="fa fa-clock-o"></i> Expired</span></a>';
	} else { // normal like button
		$output = '<span class="post-expires">';
		$output .= '<a href="#" data-post_id="'.$post_id.'">';
		$output .= '<span class="unliker"></span><span class="like"><i class="fa fa-clock-o"></i> Expired?</span>';
		$output .= '</a></span>';
	}
	
	return $output;
}

//Function: Gets the number of Post Views to be used later.
function get_postexpires($post_ID){
    $count_key = '_post_expires_count';
    //Returns values of the custom field with the specified key from the specified post.
    $count = get_post_meta($post_ID, $count_key, true);
 
    return $count;
}
//Function: Add/Register the Non-sortable 'Views' Column to your Posts tab in WP Dashboard.
function post_column_views($newcolumn){
    //Retrieves the translated string, if translation exists, and assign it to the 'default' array.
    $newcolumn['post_expires'] = __('Expired Reports');
    return $newcolumn;
}
//Hooks a function to a specific filter action.
//Applied to the list of columns to print on the manage posts screen.
add_filter( 'manage_posts_columns', 'post_column_views' );
 
//Function: Populate the 'Views' Column with the views count in the WP dashboard.
function post_custom_column_views($column_name, $id){   
    if($column_name === 'post_expires'){
        // Display the Post View Count of the current post.
        // get_the_ID() - Returns the numeric ID of the current post.
        echo get_postexpires(get_the_ID());
    }
}
//Hooks a function to a specific action. 
//allows you to add custom columns to the list post/custom post type pages.
//'10' default: specify the function's priority.
//'2' is the number of the functions' arguments.
add_action('manage_posts_custom_column', 'post_custom_column_views',10,2);
 
/**UP TO HERE IS THE SAME AS CODE-3. NEXT IS WHAT IS REQUIRED FOR SORTABLE 'VIEWS' COLUMN**/
 
//Function: Register the 'Views' column as sortable in the WP dashboard.
function register_post_column_views_sortable( $newcolumn ) {
    $newcolumn['post_expires'] = 'post_expires';
    return $newcolumn;
}
add_filter( 'manage_edit-post_sortable_columns', 'register_post_column_views_sortable' );
 
//Function: Sort Post Views in WP dashboard based on the Number of Views (ASC or DESC).
function sort_views_column( $vars ) 
{
    if ( isset( $vars['orderby'] ) && 'post_expires' == $vars['orderby'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => '_post_expires_count', //Custom field key
            'orderby' => 'meta_value_num') //Custom field value (number)
        );
    }
    return $vars;
}
add_filter( 'request', 'sort_views_column' );

?>