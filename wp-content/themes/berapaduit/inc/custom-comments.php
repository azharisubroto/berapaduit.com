<?php
/**
 * Comment layout.
 *
 * @package understrap
 */

// Comments form.
add_filter( 'comment_form_default_fields', 'bootstrap3_comment_form_fields' );

/**
 * Creates the comments form.
 *
 * @param string $fields Form fields.
 *
 * @return array
 */
function bootstrap3_comment_form_fields( $fields ) {
	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = ( $req ? " aria-required='true'" : '' );
	$html5     = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;
	$fields    = array(
		'author' => '<div class="form-group comment-form-author"><label for="author">' . __( 'Name',
				'understrap' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		            '<input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . '></div>',
		'email'  => '<div class="form-group comment-form-email"><label for="email">' . __( 'Email',
				'understrap' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		            '<input class="form-control" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . '></div>',
		'url'    => '<div class="form-group comment-form-url"><label for="url">' . __( 'Website',
				'understrap' ) . '</label> ' .
		            '<input class="form-control" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30"></div>',
	);

	return $fields;
}

add_filter( 'comment_form_defaults', 'bootstrap3_comment_form' );

/**
 * Builds the form.
 *
 * @param string $args Arguments for form's fields.
 *
 * @return mixed
 */
function bootstrap3_comment_form( $args ) {
	$args['comment_field'] = '<div class="form-group comment-form-comment">
    <label for="comment">' . _x( 'Comment', 'noun', 'understrap' ) . ( ' <span class="required">*</span>' ) . '</label>
    <textarea class="form-control" id="comment" name="comment" aria-required="true" cols="45" rows="8"></textarea>
    </div>';
	$args['class_submit']  = 'btn btn-primary'; // since WP 4.1.
	return $args;
}


/*-----------------------------------------------------------------------------------*/
// Custom Comment Structure
/*-----------------------------------------------------------------------------------*/
function zl_custom_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<footer class="comment-meta">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment->comment_author_email, 55 ); ?>
				<b class="fn"><?php printf(__('%s', 'zatolab'), get_comment_author_link()) ?></b> <span class="says">says:</span>					
			</div><!-- .comment-author -->

			<div class="comment-metadata">
				<a href="<?php comment_link(); ?>">
					<i class="fa fa-clock-o"></i>	
					<time>
						<?php comment_date() ?>, <?php comment_time('H:i:s'); ?>
					</time>
				</a>
			</div><!-- .comment-metadata -->

		</footer>
		<div class="">
			<div id="comment-<?php comment_ID(); ?>">
				<div class="zl_comment_content">
					<?php comment_text();
					if ($comment->comment_approved == '0') : ?>
						<p><em>Your comment is awaiting moderation.</em></p>
					<?php endif; ?>
				</div>

				<div class="clear"></div>

				<?php
					$class = '';
					if( ! is_user_logged_in() ) {
						$class = 'loggedout';
					}
				?>
				<div class="zl_links <?php echo esc_attr($class); ?>">
					<?php comment_reply_link(array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
					<?php if(function_exists('up_down_comment_votes')) { up_down_comment_votes( get_comment_ID() ); } ?>
					<a href="javascript:;" data-toggle="modal" data-target="#commentPermalink" data-permalink="<?php comment_link(); ?>"><i class="fa fa-link"></i> Permalink</a>
				</div>
			</div>
		</div>
		<div class="clear"></div>


<?php } //END CUSTOM COMMENT


function attachmentcomment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class('photocomment'); ?> id="li-comment-<?php comment_ID() ?>">

		<div class="large-2 column noleftpad">
			<?php echo get_avatar( $comment->comment_author_email, 50 ); ?>
		</div>
		<div class="large-10 column">
			<div id="comment-<?php comment_ID(); ?>">
				<div class="comment-author"><strong><?php printf(__('%s', 'zatolab'), get_comment_author_link()) ?></strong></div>
				<div class="clear"></div>
				<div>
					<?php comment_text();
					if ($comment->comment_approved == '0') : ?>
						<p><em>Your comment is awaiting moderation.</em></p>
					<?php endif; ?>
					<div class="clear"></div>
					<div class="small-5 column nogap commenttime">
						<?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?>
					</div>
					<div class="small-7 column">
						<?php comment_reply_link(array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
					</div>
					
				</div>
			</div>
		</div>
		<div class="clear"></div>


<?php } //END CUSTOM COMMENT


function add_comment_author_to_reply_link($link, $args, $comment){
 
    $comment = get_comment( $comment );
 
    // If no comment author is blank, use 'Anonymous'
    if ( empty($comment->comment_author) ) {
        if (!empty($comment->user_id)){
            $user=get_userdata($comment->user_id);
            $author=$user->user_login;
        } else {
            $author = 'Anonymous';
        }
    } else {
        $author = $comment->comment_author;
    }
 
    // If the user provided more than a first name, use only first name
    if(strpos($author, ' ')){
        $author = substr($author, 0, strpos($author, ' '));
    }
 
    // Replace Reply Link with "Reply to &lt;Author First Name>"
    $reply_link_text = $args['reply_text'];
    $link = str_replace($reply_link_text, '' . __('Reply','zatolab'), $link);
 
    return '<i class="fa fa-reply"></i> '.$link;
}
add_filter('comment_reply_link', 'add_comment_author_to_reply_link', 10, 3);

function count_user_comments_today( $uid ){
    global $wpdb;
    $today = date('Y-m-d');
    $tomorrow = date('Y-m-d', time() + 86400);
    $count = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->comments} WHERE user_id = %d", $uid, $today, $tomorrow  ));
    return $count;
}
?>
