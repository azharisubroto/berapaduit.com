<?php
/**
 * Pagination layout.
 *
 * @package understrap
 */

/**
 * Custom Pagination with numbers
 * Credits to http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
 */

if ( ! function_exists( 'understrap_pagination' ) ) :
function understrap_pagination() {
	if ( is_singular() OR is_page_template( 'page-templates/current-user-deals.php' ) ) {
		return;
	}

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if ( $wp_query->max_num_pages <= 1 ) {
		return;
	}

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**    Add current page to the array */
	if ( $paged >= 1 ) {
		$links[] = $paged;
	}

	/**    Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<nav aria-label="Page navigation"><ul class="pagination justify-content-center ">' . "\n";

	/**    Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active page-item"' : ' class="page-item"';

		printf( '<li %s><a class="page-link" href="%s">1</a></li>' . "\n",
		$class, esc_url( get_pagenum_link( 1 ) ), '1' );

		/**    Previous Post Link */
		if ( get_previous_posts_link() ) {
			printf( '<li class="page-item page-item-direction page-item-prev"><span class="page-link">%1$s</span></li> ' . "\n",
			get_previous_posts_link( '<span aria-hidden="true"><i class="fa fa-chevron-left"></i></span><span class="sr-only">Previous page</span>' ) );
		}

		if ( ! in_array( 1, $links ) ) {
			echo '<li class="page-item"></li>';
		}
	}

	// Link to current page, plus 2 pages in either direction if necessary.
	sort( $links );
	$i=0;
	/*foreach ( (array) $links as $link ) {
		//if( 0 == $i ) {
			$class = $paged == $link ? ' class="page-item"' : ' class="page-item"';
			printf( '<li %s><a href="%s" class="page-link">Page %s</a></li>' . "\n", $class,
				esc_url( get_pagenum_link( $link ) ), $link );
		//}
		
		$i++;
	}*/

	// Next Post Link.
	if ( get_next_posts_link() ) {
		printf( '<li class="page-item page-item-direction page-item-next"><span class="page-link">%s</span></li>' . "\n",
			get_next_posts_link( '<span aria-hidden="true"><i class="fa fa-chevron-right"></i></span><span class="sr-only">Next page</span>' ) );
	}

	// Link to last page, plus ellipses if necessary.
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) ) {
			echo '<li class="page-item"></li>' . "\n";
		}

		$class = $paged == $max ? ' class="active "' : ' class="page-item"';
		//printf( '<li %s><a class="page-link" href="%s" aria-label="Next"><span aria-hidden="true"></span><span class="sr-only">%s</span></a></li>' . "\n",
		//$class . '', esc_url( get_pagenum_link( esc_html( $max ) ) ), esc_html( $max ) );
	}

	echo '<li><a href="'. esc_url( get_pagenum_link( esc_html( $max ) ) ) .'" class="page-link" >'.$max.'</a></li>';

	echo '</ul></nav>' . "\n";
}

endif;

if( ! function_exists('bd_pagination') ):
	function bd_pagination( $pages = '' ) {
		if ( is_singular() OR is_page_template( 'page-templates/current-user-deals.php' ) ) {
			return;
		}

		global $wp_query;
		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );
	?>
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<?php 
					//if( $paged > 1 ):
				?>
				<li class="page-item">
					<a class="page-link" href="<?php echo esc_url( get_pagenum_link( 1 ) ); ?>">1</a>
				</li>
				<?php //endif; ?>
				<?php
					if ( get_previous_posts_link() ) {
						printf( '<li class="page-item page-item-direction page-item-prev"><span class="page-link">%1$s</span></li> ' . "\n",
						get_previous_posts_link( '<span aria-hidden="true"><i class="fa fa-chevron-left"></i></span><span class="sr-only">Previous page</span>' ) );
					} else {
						echo '<li class="page-item page-item-direction page-item-prev"><span class="page-link"><a href="javascript:;"><span aria-hidden="true"><i class="fa fa-chevron-left"></i></span><span class="sr-only">Previous page</span></a></span></li>';
					}
				?>

				<?php 
					if( $paged > 0 ):
				?>
					<li class="page-item">
						<div class="group">
							<a class="page-link allpaging" href="javascript:;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="paging-page-text">Page </span><span class="pg-nm"><?php echo $paged; ?></span></a>
							<div class="pagingpop dropdown-menu text-small scrolled">
							    <?php 
							    	$actual = $max + 1;
							    	for ($i=1; $i < $actual; $i++) { 
							    		echo '<a class="dropdown-item" href="'.esc_url( get_pagenum_link( esc_html( $i ) ) ).'">Page '.$i.'</a>';
							    	}
							    ?>
							</div>
						</div>
					</li>
				<?php
					endif;
				?>
				
				<?php
					if ( get_next_posts_link() ) {
						printf( '<li class="page-item page-item-direction page-item-next"><span class="page-link">%s</span></li>' . "\n",
							get_next_posts_link( '<span aria-hidden="true"><i class="fa fa-chevron-right"></i></span><span class="sr-only">Next page</span>' ) );
					}
				?>
				<?php 
					if( $max && $paged != $max ):
				?>
					<li class="page-item">
						<a class="page-link" href="<?php echo esc_url( get_pagenum_link( esc_html( $max ) ) ); ?>"><?php echo $max; ?></a>
					</li>
				<?php endif; ?>
			</ul>
		</nav>
	<?php
	}
endif;

if( ! function_exists('bd_pagination_2') ):
	function bd_pagination_2( $pages = '', $range = 3  ) {
		$showitems = ($range * 2)+1;

		global $paged;
		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }

	   if($pages == '')
	    {
	         global $wp_query;
	         $pages = $wp_query->max_num_pages;
	         if(!$pages)
	         {
	             $pages = 1;
	         }
	    }

	     if(1 != $pages)
	     {
	        echo "<!-- pagination -->\n";
	        echo '<nav aria-label="Page navigation example">';
			echo '<ul class="pagination justify-content-center">';
	         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li class='page-item'><a  class='page-link' href='".get_pagenum_link(1)."'>&laquo;</a></li>";
	         if($paged > 1 && $showitems < $pages) echo "<li class='page-item'><a href='".get_pagenum_link($paged - 1)."' class='page-link'>&lsaquo;</a></li>";

	         for ($i=1; $i <= $pages; $i++)
	         {
	             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
	             {
	                 echo ($paged == $i)? "<li class='active page-item'><a href='#' class='page-link'>".$i."</a></li>":"<li class='page-item'><a href='".get_pagenum_link($i)."' class='inactive page-link' >".$i."</a></li>";
	             }
	         }

	         if ($paged < $pages && $showitems < $pages) echo "<li class='page-item'><a href='".get_pagenum_link($paged + 1)."' class='page-link'>&rsaquo;</a></li>";
	         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li class='page-item'><a href='".get_pagenum_link($pages)."' class='page-link'>&raquo;</a></li>";
	         echo "</ul>\n";
	         echo "</nav>\n";
	     }
	}
endif;

// LOAD MORE DATA
add_action( 'wp_ajax_wishbone_loadmore', 'wishbone_loadmore' );
add_action( 'wp_ajax_nopriv_wishbone_loadmore', 'wishbone_loadmore' );
if(!function_exists('wishbone_loadmore')){
	function wishbone_loadmore() {

		// Set variables
		$looptype = $_POST['looptype'];
		$perpage = $_POST['perpage'];
		$targetpaged = $_POST['currentpaged'] + 1;

		$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $perpage,
			'ignore_sticky_posts' => 1,
			'paged' => $targetpaged,
		);

		// Category
		if( isset($_POST['cat']) ){
			$cat = $_POST['cat'];
			$args = array_merge( $args, array( "cat" => $cat ) );
		}

		// Year
		if( isset( $_POST['year'] ) ){
			$year = $_POST['year'];
			$args = array_merge( $args, array( "year" => $year ) );
		}

		// Month
		if( isset( $_POST['month'] ) ){
			$month = $_POST['month'];
			$args = array_merge( $args, array( "monthnum" => $month ) );
		}

		// Day
		if( isset( $_POST['day'] ) ){
			$day = $_POST['day'];
			$args = array_merge( $args, array( "day" => $day ) );
		}

		// Author
		if( isset( $_POST['author'] ) ){
			$author = $_POST['author'];
			$args = array_merge( $args, array( "author" => $author ) );
		}

		// Search
		if( isset( $_POST['search'] ) ){
			$search = $_POST['search'];
			$args = array_merge( $args, array( "s" => $search ) );
		}

		// Tag
		if( isset( $_POST['tag_id'] ) ){
			$tag_id = $_POST['tag_id'];
			$args = array_merge( $args, array( "tag_id" => $tag_id ) );
		}

		// Order By
		if( $_POST['orderby'] ) {
			$args = array_merge( $args, array( "orderby" => $_POST['orderby'] ) );
		}

		// Days Ago post
		if( $_POST['daysago'] ) {
			$today = getdate();
			$age =  (string)$_POST['daysago'];
			$daysago = $age." days ago";
			$args = array_merge( $args, array( 
					'date_query'     => array(
	                    array(
                            'column' => 'post_date_gmt',
							'after'  => $daysago,
                        ),
	                )
				)
			);
		} 

		// Only if have comment
		if( isset( $_POST['discussed'] ) && 'yes' == $_POST['discussed'] ) {
			add_filter( 'posts_where', 'my_has_comments_filter' );
		}

		$ajaxresults = new WP_Query( $args );

		// Only if have comment
		if( isset( $_POST['discussed'] ) && 'yes' == $_POST['discussed'] ) {
			remove_filter( 'posts_where', 'my_has_comments_filter' );
		}

		ob_start();
		echo '<div class="paging-detect" data-paged="'.$targetpaged.'">';
		if( $ajaxresults->have_posts() ):
			while( $ajaxresults->have_posts() ): $ajaxresults->the_post();
				get_template_part( 'loop-templates/content', 'item' );
			endwhile;
			wp_reset_postdata();
		endif;
		echo '</div>';
		$html = ob_get_clean();

		$response = array(
	        'sucess' => true,
	        'html' => $html,
	    );

	    // generate the response
	    print json_encode($response);
	    // IMPORTANT: don't forget to "exit"
	    exit;
	}
}

/**
 * Render Ajax Pagination (Load More)
 * @since wishbone 1.0
 */
function wishbone_ajax_pagination( $looptype = null, $post=null ){
	global $wp_query;
	global $paged;
	$search = $author = $datayear = $datamonth = $dataday = $dataauthor = $datasearch = $datatag = $tag = '';
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	if( is_page_template( 'template-home.php' ) ){
		$maxpages = $post;
	} else {
		$maxpages = $wp_query->max_num_pages;
	}


	/*'day' => $currentday, //tanggal
	'year' => $currentyear, //tahun
	'monthnum' => $currentmonth, //bulan*/


	//Category
	if( is_category() ){
		$cat_id = 'data-cat="'.get_query_var('cat').'"';
	} else {
		$cat_id = null;
	}

	//Archive
	if( is_archive() ){

		$year = get_the_time('Y');
		$month = get_the_time('m');
		$day = get_the_time('j');

		if( is_year() ){
			$datayear = 'data-year="'.$year.'"';
		} elseif( is_month() ) {
			$datayear = 'data-year="'.$year.'"';
			$datamonth = 'data-month="'.$month.'"';
		} elseif( is_day() ) {
			$datayear = 'data-year="'.$year.'"';
			$datamonth = 'data-month="'.$month.'"';
			$dataday = 'data-day="'.$day.'"';
		}

	}

	// Author
	if( is_author() ){
		$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
		$dataauthor = 'data-author="'.$author->ID.'"';
	}

	// Search
	if( is_search() ){
		$search = get_search_query();
		$datasearch = 'data-search="'.$search.'"';
	}

	// Tag
	if( is_tag() ){
		$tag = get_query_var('tag_id');
		$datatag = 'data-tag="'.$tag.'"';
	}

	if( $paged != $maxpages ) {
	?>
		<a href="#" class="wishbone_loadmorebutton"
		data-perpage="<?php echo esc_attr( get_option('posts_per_page') ); ?>"
		data-currentpaged="<?php echo esc_attr( $paged ); ?>"
		data-maxpages="<?php echo esc_attr( $maxpages );  ?>"
		<?php echo $cat_id.' '.$datayear.' '.$datamonth.' '.$dataday.' '.$dataauthor.' '.$datasearch.' '.$datatag; ?>>
			<?php esc_html_e( 'LOAD MORE', 'jt-wishbone' ); ?>
		</a>
	<?php
	}
}