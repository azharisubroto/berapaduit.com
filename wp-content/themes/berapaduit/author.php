<?php get_header();
global $wp;
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
$authormeta = $tab = '';
if (is_author()){
    $current_user = get_queried_object();
    $user = $current_user->ID;
    $authorname = get_the_author_meta( 'first_name', $current_user->ID );
    $authorlastname = get_the_author_meta( 'last_name', $current_user->ID );
}

if( isset($_GET["tab"]) ) {
	$tab = $_GET["tab"];
}

$current_url = get_author_posts_url( $user );
?>

<div class="container-fluid pb-3 pt-3 user-cover text-white">
	<div class="user-cover-placeholder"></div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="float-left up-ava mr-4">
					<?php echo get_avatar( $current_user->ID , 120 ); ?>
				</div>
				<div class="float-left pt-4">
					<div>
						<h5><?php echo $authorname; ?> <?php echo $authorlastname; ?></h5>
					</div>
					<div class="small">
						<i class="fa fa-calendar"></i>
						<?php 
							$udata = get_userdata( $current_user->ID );

				            $registered = $udata->user_registered;

				            printf( 'Joined since %s', date( "d M Y", strtotime( $registered ) ) );

				            if ( berapaduit_is_user_online($user) ){
				            	echo '<span class="badge badge-light pt-2 pb-2 ml-2"><i class="fa fa-circle text-green"></i> Online Now</span>';
				            } else {
				            	echo '<span class="badge badge-light pt-2 pb-2 ml-2"><i class="fa fa-circle"></i> Offline</span>';
				            }
						 ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ******************* 2nd Navbar Area ******************* -->
<div class="wrapper wrapper-navbar subnav pt-0 pb-0">
	<nav class="navbar navbar-expand-md navbar-light bd-bg-grey">
		<div class="container">
			<div id="navbarNavDropdown" class="navbar-nav-scroll">
				<ul id="secondary-menu" class="navbar-nav bd-navbar-nav flex-row">
					<li class="menu-item nav-item <?php if( 'overview' == $tab ) echo 'active'; ?>"><a title="Overview" href="<?php echo esc_url( $current_url ); ?>/?tab=overview" class="nav-link">Overview</a></li>

					<li class="menu-item nav-item <?php if( '' == $tab ) echo 'active'; ?>"><a title="Deals" href="<?php echo esc_url( $current_url ); ?>" class="nav-link">Deals</a></li>

					<li class="menu-item nav-item <?php if( 'discussion' == $tab ) echo 'active'; ?>"><a title="Discussions" href="<?php echo esc_url( $current_url ); ?>/?tab=discussion" class="nav-link">Discussions</a></li>
				</ul>
			</div>			
		</div>
	</nav>
</div>

<div class="wrapper" id="wrapper-index">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		
		<?php if( 'overview' == $tab ){ ?>

			<div class="row sg">

				<!-- oooooooooooooooooooooooooooooooooooooooooooooooooooo
				USER STATISTIC
				ooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
				<div class="col-md-3">
					<?php bd_user_sidebar( $user ); ?>
				</div>

				<!-- oooooooooooooooooooooooooooooooooooooooooooooooooooo
				ACTIVITY
				ooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
				<div class="col-md-9">
					<ul class="nav nav-tabs nav-fill activitytab" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active pt-3 pb-3" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-briefcase"></i> Activity</a>
						</li>
						<li class="nav-item">
							<a class="nav-link pt-3 pb-3" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-star"></i> Badges</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane active card" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="card-body">
								<?php bd_user_acticity( $user ); ?>
							</div>
						</div>
						<div class="tab-pane card" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<div class="card-body">
								<div class="pt-5 pr-3 pb-5 pl-5q text-center">
									<div class="inline">
										<img src="<?php echo get_template_directory_uri(); ?>/img/sad.png" alt="">
									</div>
									<div class="ml-3 inline text-left">
										<strong>User's Badge is not ready for now</strong><br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		<?php } elseif( 'discussion' == $tab ) { ?>
			<div class="row">

				<!-- Do the left sidebar check and opens the primary div -->
				<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

				<main class="site-main" id="main">

					<?php 
						global $current_user;
						wp_get_current_user();
						if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
						$author_query = array('author'=> $user, 'post_type' => 'discussion', 'paged' =>  $paged);
						$author_posts = new WP_Query($author_query);
					 ?>

					<?php if ( $author_posts->have_posts() ) : ?>

						<?php /* Start the Loop */ ?>

						<?php while ( $author_posts->have_posts() ) : $author_posts->the_post(); ?>

							<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'loop-templates/content', 'discussion' );
							?>

						<?php endwhile; wp_reset_postdata(); ?>
						<?php bd_pagination_2( $author_posts->max_num_pages ); ?>

					<?php else : ?>

						<?php get_template_part( 'loop-templates/content', 'none' ); ?>

					<?php endif; ?>

				</main><!-- #main -->

				<!-- The pagination component -->
				<?php // understrap_pagination(); ?>

			</div><!-- #primary -->

			<!-- Do the right sidebar check -->
			<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

				<?php get_sidebar( 'right' ); ?>

			<?php endif; ?>
		<?php } else { ?>

			<div class="row">

				<!-- Do the left sidebar check and opens the primary div -->
				<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

				<main class="site-main" id="main">

					<?php if ( have_posts() ) : ?>

						<div class="paging-detect"  data-paged="1">
							<?php /* Start the Loop */ ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'loop-templates/content', 'item' );
								?>
							<?php endwhile; ?>
						</div>

						<?php if( function_exists('wishbone_ajax_pagination') ) wishbone_ajax_pagination(); ?>

					<?php else : ?>

						<?php get_template_part( 'loop-templates/content', 'none' ); ?>

					<?php endif; ?>

				</main><!-- #main -->

				<!-- The pagination component -->
				<?php // understrap_pagination(); ?>

			</div><!-- #primary -->

			<!-- Do the right sidebar check -->
			<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>
				<?php get_sidebar( 'right' ); ?>
			<?php endif; ?>

		<?php } ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
