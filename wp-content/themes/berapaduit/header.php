<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
	$authorname = $authorlastname = $altname = '';
	// IF USER LOGGED IN
	if( is_user_logged_in() ) {
		global $current_user;
		wp_get_current_user();
		$user = get_current_user_id();
		$authorname = get_the_author_meta( 'first_name', $user );
		$authorlastname = get_the_author_meta( 'last_name', $user );

		if( empty($authorname) && empty($authorlastname) ) {
			$finalname = get_the_author_meta( 'user_nicename', $user );
		} else {
			$finalname = $authorname.' '.$authorlastname;
		}
	}
 ?>

<?php 
	// LOGIN MODAL, IF USER IS NOT LOGGED
	if( ! is_user_logged_in() ):
 ?>
	<div class="modal fade bd-login-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fa fa-times"></i></span>
				</button>
				<?php bd_loginform(); ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div class="wrapper wrapper-navbar pb-0" id="wrapper-navbar">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
		'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-dark bg-bd">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

				<!-- Your site title as branding in the menu -->
				<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
				</a>
				<!-- end custom logo -->

				<div class="d-block d-md-none m-utilities">
					<?php if( !is_user_logged_in() ): ?>

						<a href="<?php echo esc_url( home_url( '/' ) ); ?>login" class="btn btn-primary bd-btn-primary loginmodal" data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-user-o mr-0"></i></a>

						<a href="#" class="btn btn-warning bd-btn-warning" data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-plus mr-0"></i></a>

					<?php else: ?>

						<!-- ooooooooooooooo NOTIFICATION oooooooooooooooo -->
						<div class="dropdown notifwrapper notif-only">
							<a href="#" class="btn text-white pr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell mr-0 ml-0"></i>
							</a>
							<div class="dropdown-menu">
								<h6 class="dropdown-header"><i class="fa fa-bell"></i> Notifications</h6>
								<div class="dropdown-divider"></div>
								<div class="scrolled notif-feed">
									<?php bd_notifications_feed(); ?>
								</div>
							</div>
						</div>

						<!-- ooooooooooooooo USER MENU oooooooooooooooo -->
						<div class="dropdown notifwrapper">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>my-profile"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="user-menu">
									<?php echo get_avatar( $current_user->ID , 40 ); ?>
								</span>
							</a>
							<div class="dropdown-menu">
								<h6 class="dropdown-header text-center text-primary"><?php echo $finalname; ?></h6>
								<div class="dropdown-divider"></div>
								<div class="scrolled notif-feed">
									<?php wp_nav_menu(
										array(
											'theme_location'  => 'profile',
											'container_class' => '',
											'container_id'    => 'user-dropdown',
											'menu_class'      => 'lex-row',
											'fallback_cb'     => '',
											'menu_id'         => 'user-menu-2',
											'walker'          => new WP_Bootstrap_Navwalker(),
										)
									); ?>
								</div>
							</div>
						</div>

						<!-- <a href="<?php echo esc_url( home_url( '/' ) ); ?>submit-form" class="btn btn-warning bd-btn-warning"><i class="fa fa-plus"></i> Submit</a> -->
						<div class="dropdown">
							<button class="btn btn-warning bd-btn-warning" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-plus mr-0"></i>
							</button>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="<?php echo esc_url( home_url('/') ); ?>submit-form"><i class="fa fa-check"></i> Deals</a>
								<a class="dropdown-item" href="<?php echo esc_url( home_url('/') ); ?>discussion-form/"><i class="fa fa-comments"></i> Discussion</a>
							</div>
						</div>

					<?php endif; ?>
				</div>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="bd-search-form bd-m-search-form d-md-none">
					<div class="form-control bd-search">
						<i class="fa fa-search"></i>
						<input type="search" name="s" placeholder="Search Brands, Producst, etc" class="form-control" autocomplete="off" >
						<ul class="bd-live-search">
							
						</ul>
						<a href="javascript:;" class="live-search-dismiss"><i class="fa fa-close"></i></a>
					</div>
				</form>

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'walker'          => new WP_Bootstrap_Navwalker(),
					)
				); ?>

				<!-- Accessories -->
				<div class="nav-els d-none d-md-inline-block">

					<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="bd-search-form">
						<div class="form-control bd-search">
							<i class="fa fa-search"></i>
							<input type="search" name="s" placeholder="Search Brands, Producst, etc" class="form-control" autocomplete="off" >
							<ul class="bd-live-search">
								
							</ul>
							<a href="javascript:;" class="live-search-dismiss"><i class="fa fa-close"></i></a>
						</div>
					</form>

					<?php if( !is_user_logged_in() ): ?>

						<a href="<?php echo esc_url( home_url( '/' ) ); ?>login" class="btn btn-primary bd-btn-primary loginmodal" data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-user-o"></i> Login/Register</a>

						<a href="#" class="btn btn-warning bd-btn-warning" data-toggle="modal" data-target=".bd-login-modal"><i class="fa fa-plus"></i> <span class="d-none d-lg-inline">Submit</span></a>

					<?php else: ?>

						<!-- ooooooooooooooo NOTIFICATION oooooooooooooooo -->
						<div class="dropdown notifwrapper notif-only" id="dd-1">
							<a href="#" class="btn text-white pr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell mr-0 ml-0"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<h6 class="dropdown-header"><i class="fa fa-bell"></i> Notifications</h6>
								<div class="dropdown-divider"></div>
								<div class="scrolled notif-feed">
									<?php bd_notifications_feed(); ?>
								</div>
							</div>
						</div>

						<!-- ooooooooooooooo USER MENU oooooooooooooooo -->
						<div class="dropdown notifwrapper">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>my-profile"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="user-menu">
									<?php echo get_avatar( $current_user->ID , 40 ); ?>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<h6 class="dropdown-header text-center text-primary"><?php echo $finalname; ?></h6>
								<div class="dropdown-divider"></div>
								<div class="scrolled notif-feed">
									<?php wp_nav_menu(
										array(
											'theme_location'  => 'profile',
											'container_class' => '',
											'container_id'    => 'user-dropdown',
											'menu_class'      => 'lex-row',
											'fallback_cb'     => '',
											'menu_id'         => 'user-menu',
											'walker'          => new WP_Bootstrap_Navwalker(),
										)
									); ?>
								</div>
							</div>
						</div>

						<!-- <a href="<?php echo esc_url( home_url( '/' ) ); ?>submit-form" class="btn btn-warning bd-btn-warning"><i class="fa fa-plus"></i> Submit</a> -->
						<div class="dropdown">
							<button class="btn btn-warning bd-btn-warning" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-plus"></i> <span class="d-none d-lg-inline">Submit</span>
							</button>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="<?php echo esc_url( home_url('/') ); ?>submit-form"><i class="fa fa-check"></i> Deals</a>
								<a class="dropdown-item" href="<?php echo esc_url( home_url('/') ); ?>discussion-form/"><i class="fa fa-comments"></i> Discussion</a>
							</div>
						</div>

					<?php endif; ?>
					
				</div>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->

	<?php 
		$templates = array(
			'page-templates/profile.php',
			'page-templates/current-user-deals.php',
			'page-templates/savedposts.php',
			'page-templates/profile-edit.php',
			'page-templates/current-user-discussions.php'
		);

		if( is_page_template( $templates ) ): 
			// Get User Info
			global $current_user;
			wp_get_current_user();
			$user = get_current_user_id();
	?>
		<div class="container-fluid pb-3 pt-3 user-cover text-white">
			<div class="user-cover-placeholder"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="float-left up-ava mr-4">
							<?php echo get_avatar( $current_user->ID , 120 ); ?>
							<?php // echo do_shortcode('[avatar_upload]'); ?>
						</div>
						<div class="float-left pt-4">
							<div>
								<h5><?php echo $finalname; ?></h5>
							</div>
							<div class="small">
								<i class="fa fa-calendar"></i>
								<?php 
									$udata = get_userdata( $user );

						            $registered = $udata->user_registered;

						            printf( 'Joined since %s', date( "d M Y", strtotime( $registered ) ) );
								 ?>
								<span class="badge badge-light pt-2 pb-2 ml-2"><i class="fa fa-circle text-green"></i> Online Now</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- ******************* 2nd Navbar Area ******************* -->
		<div class="wrapper wrapper-navbar subnav pt-0 pb-0">
			<nav class="navbar navbar-expand-md navbar-light bd-bg-grey">
				<div class="container">
					<?php wp_nav_menu(
						array(
							'theme_location'  => 'profile',
							'container_class' => 'navbar-nav-scroll',
							'container_id'    => 'navbarNavDropdown2',
							'menu_class'      => 'navbar-nav bd-navbar-nav flex-row',
							'fallback_cb'     => '',
							'menu_id'         => 'secondary-menu',
							'walker'          => new WP_Bootstrap_Navwalker(),
						)
					); ?>
				</div>
			</nav>
		</div>
	<?php endif; ?>
	
	<?php if( is_home() || is_category() || is_single() || is_page_template( 'page-templates/shortcodeable.php' ) ): ?>
	<!-- ******************* 2nd Navbar Area ******************* -->
	<div class="wrapper wrapper-navbar subnav pt-0 pb-0">
		<nav class="navbar navbar-expand-md navbar-light bd-bg-grey">
			<div class="container">
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'secondary',
						'container_class' => 'navbar-nav-scroll',
						'container_id'    => 'navbarNavDropdown3',
						'menu_class'      => 'navbar-nav bd-navbar-nav flex-row',
						'fallback_cb'     => '',
						'menu_id'         => 'secondary-menu',
						'walker'          => new WP_Bootstrap_Navwalker(),
					)
				); ?>
				
				<?php if( is_archive() || is_home() || is_page_template( 'page-templates/shortcodeable.php' ) ): ?>
				<div class="dropdown filtering d-none d-lg-block">
					<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter"></i> Filter
					</a>

					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
						<div class="dropdown-item">
							Layout: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
						<div class="pl-3 pr-3 pt-2 layoutfilter">
							<a href="#" class="active" data-layout="default"><i class="fa fa-th-list"></i></a>
							<a href="#" data-layout="grid"><i class="fa fa-th-large"></i></a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</nav>
	</div>
	<?php endif; ?>