<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div class="wrapper wrapper-navbar pb-0" id="wrapper-navbar">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
		'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-dark bg-bd">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

				<!-- Your site title as branding in the menu -->
				<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
				</a>
				<!-- end custom logo -->

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'walker'          => new WP_Bootstrap_Navwalker(),
					)
				); ?>

				<!-- Accessories -->
				<div class="nav-els">
					<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="bd-search-form">
						<div class="form-control bd-search">
							<i class="fa fa-search"></i>
							<input type="search" name="s" placeholder="Search Brands, Producst, etc" class="form-control">
						</div>
					</form>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>/login" class="btn btn-primary bd-btn-primary"><i class="fa fa-user-o"></i> Login/Register</a>
					<a href="#" class="btn btn-warning bd-btn-warning"><i class="fa fa-plus"></i> Submit</a>
				</div>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->
	
	<?php if( ! is_page_template( 'page-templates/login.php' ) ): ?>
	<!-- ******************* 2nd Navbar Area ******************* -->
	<div class="wrapper wrapper-navbar subnav pt-0 pb-0">
		<nav class="navbar navbar-expand-md navbar-light bd-bg-grey">
			<div class="container">
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'secondary',
						'container_class' => 'navbar-nav-scroll',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav bd-navbar-nav flex-row',
						'fallback_cb'     => '',
						'menu_id'         => 'secondary-menu',
						'walker'          => new WP_Bootstrap_Navwalker(),
					)
				); ?>
				
				<a href="#" class="btn btn-outline-primary"><i class="fa fa-filter"></i> Filter</a>
			</div>
		</nav>
	</div>
	<?php endif; ?>