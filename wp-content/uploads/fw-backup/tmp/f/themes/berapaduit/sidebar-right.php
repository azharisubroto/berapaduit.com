<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! is_active_sidebar( 'right-sidebar' ) ) {
	return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( 'both' === $sidebar_pos ) : ?>
<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">
	<?php else : ?>
<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">
	<?php endif; ?>

<aside class="card bd-card mb-3">
	<div class="card-body pt-3">
		<ul class="inline-list equalwidth">
			<li class="text-left">Hottest</li>
			<li class="text-center">
				<div class="fakebox">
					<select name="" id="" class="nostyle">
						<option value="today">Today</option>
						<option value="week">Week</option>
					</select>
				</div>
			</li>
			<li class="text-right">
				<a href="#"><i class="fa fa-gear"></i></a>
			</li>
		</ul>
		<div class="clearfix"></div>
		<?php
			// The Query
			$args = array(
			    'posts_per_page' => 5,          
			    'ignore_sticky_posts' => 1,          
			);
			$the_query = new WP_Query( $args );

			// The Loop
			if ( $the_query->have_posts() ) {
				echo '<ul class="no-list-style ml-0 pl-0 mt-3 hotpost">';
				while ( $the_query->have_posts() ) { $the_query->the_post(); 
			?>
				<li class="small">
					<div class="inner">
						<div class="row">
							<div class="col-md-4">
								<a href="<?php the_permalink(); ?>" class="netralclr small"><?php the_post_thumbnail('thumbnail');?></a>
							</div>
							
							<div class="col-md-8">
								<div class="netralclr"><?php the_terms( get_the_ID(), 'deal', '', ', ', '' ); ?></div>
								<div class="clearfix votewid">
									<?php if(function_exists('up_down_post_votes')) { up_down_post_votes( get_the_ID(), false ); } ?> <i class="fa fa-flag"></i>
								</div>
								<a href="<?php the_permalink(); ?>" class="netralclr small"><?php the_title(); ?></a>
							</div>
						</div>
					</div>
				</li>
			<?php }
				echo '</ul>';
				/* Restore original Post Data */
				wp_reset_postdata();
			} else {
				echo 'No Post to show';
			}
		?>

	</div>
</aside>
<?php dynamic_sidebar( 'right-sidebar' ); ?>

</div><!-- #secondary -->
