var $ = jQuery;

$(document).ready(function($){
	$('.foogle').click(function(e){
		e.preventDefault();
		$('.footer-main').slideToggle();
	});

	$('.smoothtop').on('click', function(e) {
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "fast");
		return false;
	});

	$('#show_login').on('click', function(e){
		e.preventDefault();
		$('.signup').hide();
		$('.signin').show();
	});

	//$('.rminput input').addClass('form-control').attr('style', '');

	$('.rminput').each(function(){
		$(this).find('input').addClass('form-control').attr('style', '');
	});

	if( $('.wpuf-form-add').length ) {
		$('#datepicker_59').datepicker({
			dateFormat: 'yy-m-d',
			onSelect: function(date) {
		        $('input[name="date_expires"]').val(date);
		    }
		});

		$('#post_title_59').on('keyup', function(){
			var thisval = $(this).val();
			$('.pre-title').text(thisval);
		});

		window.onload = function () {
	        tinymce.get('post_content_59').on('keyup',function(e){
	        	//console.log(this.getContent());
	        	$('.entry-content').html(this.getContent());
	        });
    	}
	}

	if( $('#stickysidebar') && $(window).width() >= 860 ) {
		var footerheight = $('.footer-wrapper').outerHeight();
		var footerheight = footerheight + 15;
		$("#stickysidebar").sticky({
			topSpacing:100,
			bottomSpacing: footerheight
		});
	}
});