<?php
/**
 * Loop template
 *
 * @package understrap
 */

?>
<article <?php post_class('card bd-card mb-3'); ?> id="post-<?php the_ID(); ?>">	
	<div class="card-body">
		<div class="row">
			<div class="col-md-3">
				<div class="item-thumb">
					<?php the_post_thumbnail('medium'); ?>
				</div>
			</div>
			<div class="col-md-9">
				<div class="row mb-2">
					<div class="col-md-4">
						<?php if(function_exists('up_down_post_votes')) { up_down_post_votes( get_the_ID() ); } ?>
					</div>
					<div class="col-md-8 text-right fadetext small">
						<i class="fa fa-clock-o"></i> <?php printf( _x( '%s ago', '%s = human-readable time difference', 'your-text-domain' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
					</div>
				</div>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="clearfix entry-meta">
					<?php the_terms( get_the_ID(), 'deal', '', ', ', '' ); ?> Deal
				</div>
				<div class="entry-content">
					<p><?php echo bd_excerpt( 23 ); ?></p>
				</div>
				<div class="entry-footer">
					<div class="inner">
						<div class="row">
							<div class="col-md-4">
								<div class="ava inline mr-1">
									<?php echo get_avatar( get_the_author_meta('ID') , 25 ); ?>
								</div>
								<?php the_author_posts_link(); ?>
							</div>
							<div class="col-md-8 text-right">
								<a href="<?php the_permalink(); ?>#comments" class="btn btn-sm btn-outline-dark bd-btn-outline-dark"><i class="fa fa-comment"></i> <?php echo get_comments_number(); ?></a>

								<a href="<?php the_field('external_link'); ?>" class="btn btn-sm btn-warning bd-btn-warning" target="_blank">Get Deals <i class="fa fa-shopping-bag mr-0"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>