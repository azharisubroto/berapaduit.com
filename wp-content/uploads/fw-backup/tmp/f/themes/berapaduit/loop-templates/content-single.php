<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="card bd-card rb-0">
		<div class="card-body">
			<div class="row">
				<div class="col-md-3">
					<div class="item-thumb">
						<?php 
							the_post_thumbnail('medium');
						 ?>
					</div>
				</div> <!-- .col-md-3 -->
				<div class="col-md-9">
					<div class="row mb-2">
						<div class="col-md-12">
							<?php if(function_exists('up_down_post_votes')) { up_down_post_votes( get_the_ID() ); } ?>
						</div>
					</div> <!-- .row.mb-2 -->

					<h2 class="entry-title"><?php the_title(); ?></h2>

					<div class="clearfix entry-meta">
						<?php 
							$terms = get_terms('deal');
							foreach ($terms as $term) {
							    echo '<a href="'.get_term_link($term).'">'.$term->name.'</a>';
							}
						 ?> Deal
					</div> <!-- .entry-meta -->

					<div class="entry-footer">
						<div class="inner">
							<div class="row">
								<div class="col-md-4">
									<div class="ava inline mr-1">
										<?php echo get_avatar( get_the_author_meta('ID') , 25 ); ?>
									</div> <!-- .ava -->
									<?php the_author_posts_link(); ?>
								</div> <!-- .col-md-4 -->
								<div class="col-md-8 text-right">
									<a href="<?php the_permalink(); ?>#comments" class="btn btn-sm btn-outline-dark bd-btn-outline-dark"><i class="fa fa-comment"></i> <?php echo get_comments_number(); ?></a>
									<a href="<?php the_field('external_link'); ?>" class="btn btn-sm btn-warning bd-btn-warning" target="_blank">Get Deals <i class="fa fa-shopping-bag mr-0"></i></a>
								</div> <!-- .col-md-8 -->
							</div> <!-- .row -->
						</div> <!-- .inner -->
					</div> <!-- .entry-footer -->
				</div> <!-- .col-md-9 -->
			</div> <!-- .row -->
		</div> <!-- .card-body -->
	</div> <!-- .card.bd-card -->

	<div class="card bd-card rt-0 rb-0">
		<div class="card-body pt-0"> 
			<!-- Meta -->
			<div class="single-meta mb-2">
				<span>
					<i class="fa fa-pencil-square-o"></i> Edited by: <?php the_author(); ?> on <?php the_modified_date('F j, Y'); ?>
				</span>
				<span>
					<i class="fa fa-tag"></i> Found 14 Nov
				</span>
			</div> <!-- .single-meta -->

			<!-- Content -->
			<div class="entry-content mb-0">
				<?php the_content(); ?>
			</div> <!-- .entry-content -->

			<!-- Share -->
			<div class="sharer mb-3">
				<ul class="ball-link inline-list">
					<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_permalink() ) ?>" class="fb" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://twitter.com/home?status=<?php echo esc_url( get_permalink() ) ?>" class="twit" target="_blank"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div> <!-- .sharer -->
			
			<!-- Tags -->
			<div class="tags fadetext mb-3">
				<?php the_tags('', ' '); ?>
			</div> <!-- .tags -->

			<!-- entry-footer -->
			<div class="entry-single-footer">
				<a href="#"><i class="fa fa-comment"></i>New Comment</a>
				<a href="#"><i class="fa fa-eye"></i>Subscribe</a>
				<a href="#"><i class="fa fa-clock-o"></i>Expired?</a>
				<a href="#"><i class="fa fa-exclamation-circle"></i>Report Thread</a>
				<a href="#"><i class="fa fa-bookmark"></i>Save for Later</a>
				<a href="#"><i class="fa fa-thumb-tack"></i>Embed</a>
			</div>
		</div> <!-- .card-body -->
	</div>	<!-- .card bd-card -->
</article><!-- #post-## -->
